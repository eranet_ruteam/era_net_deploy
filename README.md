## Project structure ##


```
├── config (camera ini-file)
├── era_tools
|    ├── calibrator (camera & lidar calibration tools docker)
|    └── mstate (state machine daemon & interface)
|    └── calibtation_tool_no docker  (calibration without docker)
|    └── sythetic_dataset_maker_tool  (artifitial dataset maker)

├── mrcnn-ppf (docker & run files for Mask R-CNN)
|    ├── data (weights, models and imgs)
|    └── src (https://bitbucket.org/eranet_ruteam/mrcnn-ppf/src/master/)
├── pose (docker & run files for PoseCNN)
|    ├── data (input & output data for PoseCNN testing & training)
|    └── yml-dev (developing docker-compose run scripts)
```

## Requirements ##

* Nvidia driver (if you haven`t done it yet)
* Switch to Nvidia GPU card if there are several (```sudo nvidia-settings```)
* [*docker-ce*](https://docs.docker.com/install/linux/docker-ce/ubuntu/#uninstall-old-versions)
* [*nvidia-docker2*](https://github.com/NVIDIA/nvidia-docker)
* [*docker-compose*](https://docs.docker.com/compose/install/#prerequisites)

## Important ##
If tkInter visualization (version 1.2.1 and 1.3 of docker-image)

* *x11-xserver-utils* and command `xhost +`
* **Do mapping of the host nvidia-drivers to docker: change in docker-compose-[video|webcam].yml file pattern `nvidia-410` to `nvidia-XYZ` where XYZ is host nvidia-driver version**

### MRCNN runs is described in [*mrcnn-ppf*](https://bitbucket.org/eranet_ruteam/era_net_deploy/src/master/mrcnn-ppf/) directory ###

### POSE runs is described in [*pose*](https://bitbucket.org/eranet_ruteam/era_net_deploy/src/master/pose/) directory ###

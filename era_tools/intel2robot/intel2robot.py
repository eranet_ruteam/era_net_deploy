import json
import sys
import numpy as np
# result of calibration from, rootcalibrator_r.py

def make_transform(alpha, beta, gamma, Dx=0., Dy=0., Dz=0.):
    flip_transform_x = [[1, 0, 0, 0],
                        [0, np.cos(alpha), -np.sin(alpha), 0],
                        [0, np.sin(alpha), np.cos(alpha), 0],
                        [0, 0, 0, 1]]

    flip_transform_y = [[np.cos(beta), 0, np.sin(beta), 0],
                        [0, 1, 0, 0],
                        [-np.sin(beta), 0, np.cos(beta), 0],
                        [0, 0, 0, 1]]

    flip_transform_z = [[np.cos(gamma), -np.sin(gamma), 0, 0],
                        [np.sin(gamma), np.cos(gamma), 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1]]

    flip_tot = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    flip_tot = np.dot(flip_tot, flip_transform_y)
    flip_tot = np.dot(flip_tot, flip_transform_x)
    flip_tot = np.dot(flip_tot, flip_transform_z)

    return flip_tot


def main(ix = 100, iy = 0, iz = 0, rx = 0 , ry =0 , rz = 0 , A =  45 , B = 0, C = -90 ):

    global inputname_config
    global center_pt_calibrated

    param = sys.argv
    if(len(param) ==10 ):

# координаты того,что отдала система тех зрения в системе координат камеры
        ix = param[1]
        iy = param[2]
        iz = param[3]

# координаты руки робота
        rx = param[4]
        ry = param[5]
        rz = param[6]
        A = param[7]
        B = param[8]
        C = param[9]


    shift_cam = [ix, iy, iz, 0]
    point = [rx, ry, rz, 0]

# перестанока осей к координате робота
    intel2robot = np.asanyarray([[0, 1, 0, 0],
                                 [0, 0, 1, 0],
                                 [1, 0, 0, 0],
                                 [0, 0, 0, 1]])

    shift = np.dot(shift_cam, intel2robot)

    a = A / 180 * np.pi
    b = B /180 * np.pi
    c = C / 180 * np.pi
# поворот координат камеры к координатной системе робота
    i1r = make_transform(0, 0, -a)
    i2r = make_transform(0, -b, 0)
    i3r = make_transform(-c, 0, 0)

    shift = np.dot(shift, i1r)
    shift = np.dot(shift, i2r)
    shift = np.dot(shift, i3r)

    point[0] = point[0] + shift[0]
    point[1] = point[1] + shift[1]
    point[2] = point[2] + shift[2]

# тут уже в координатах робота координаты объекта
    print(point[0],point[1],point[2])

    exit(0)

if __name__ == "__main__":
    main()
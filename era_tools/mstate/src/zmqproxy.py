import os
import zmq
import configparser

from settings import config_path

def main():

    config = configparser.ConfigParser()
    config.read(os.path.join(config_path, 'config.ini'))
    proxy = config['proxy']

    context = zmq.Context()

    # Socket facing producers
    frontend = context.socket(zmq.XPUB)
    frontend.bind("tcp://*:{}".format(proxy['PORT_PUB']))

    # Socket facing consumers
    backend = context.socket(zmq.XSUB)
    backend.bind("tcp://*:{}".format(proxy['PORT_SUB']))

    zmq.proxy(frontend, backend)

    # We never get here…
    frontend.close()
    backend.close()
    context.term()

if __name__ == "__main__":
    main()
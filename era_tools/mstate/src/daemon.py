import os
import sys
import calendar
import time
import json

from settings import config_path

from helpers import Poller, Publisher

class StateMachine:
    def __init__(self, data, timeout=100):
        self.timeout = int(timeout)
        self.data = data[1:]  # First field is informational
        self.zero_state()

    def _get_reference(self):
        """ whole states objects in the tuple of tuples (device, result, state) """
        devices, states = self._coerce_ref_matrix()
        for state, results in enumerate(states):
            step_state = list()
            for i, result in enumerate(results):
                step_state.append((devices[i], result, state))
            yield tuple(step_state)

    def zero_state(self):
        """ switch to initial state """
        self.reference_matrix = self._get_reference()
        self.current = []
        self.state = 0
        self.switch_time = self._current_time()

    def next_state(self):
        """ move to the next state and ignore None results """
        self.current = list(next(self.reference_matrix))
        self.state = self.current[0][2]
        self._coerce_cur_matrix()
        self.switch_time = self._current_time()

    def check_device_result(self, message):
        """
        delete successful mach from current_matrix
        :param message: dict.keys('name', 'result') 
        :return: 
        """
        pattern = (message['name'], message['result'], self.state)
        self.current = [data for data in self.current if data != pattern]

    def is_ready_to_next(self):
        """ if current_matrix zero-lenght """
        if not self.current:
            return True

    def get_state(self):
        """ current machine state """
        return self.state

    def get_devs(self):
        """
        devices with not None expectation
        for current machine state 
        :return: list
        """
        devices = []
        for reference_state in self.current:
            if reference_state[1] is not None:
                devices.append(reference_state[0])
        return devices

    def check_timeout(self):
        """ time since state was changed """
        if self._current_time() - self.switch_time > self.timeout:
            return True

    def _coerce_ref_matrix(self):
        data = list(zip(*self.data))
        return data[0], data[1:]

    def _coerce_cur_matrix(self):
        self.current = [data for data in self.current if data[1] is not None]

    def _current_time(self):
        return calendar.timegm(time.gmtime())


def main():
    try:
        with open(os.path.join(config_path, 'data.json'), 'r') as fp:
            data = json.load(fp)
    except FileNotFoundError:
        print("Configure state machine firstly")
        exit()

    # getting only topic "2" (from dev`s) messages from subscriber
    poller = Poller("2")
    publisher = Publisher()
    try:
        sm = StateMachine(data['matrix'], poller.config['DEFAULT']['TIMEOUT'])
    except KeyError:
        sm = StateMachine(data['matrix'])
        
    sm.next_state()
    while True:
        time.sleep(2)
        sys.stdout.write('Current state: {}\n'.format(sm.get_state()))
        sys.stdout.write('Expected devices: {}\n\n'.format(sm.get_devs()))
        message = {'name': 'daemon', 'result': sm.get_devs()}

        if sm.check_timeout():
            sm.zero_state()
            sm.next_state()
            sys.stdout.write('----------> Go to state 1 by timeout\n\n')
            publisher.send_message(1, message)

        # get message if Subscriber sent it
        try:
            dev_message = poller.recv_zipped_pickle()
            sm.check_device_result(dev_message)
            if sm.is_ready_to_next():
               sm.next_state()
               sys.stdout.write('-----> Go to next state: {}\n'.format(sm.get_state()))
               sys.stdout.write('Expected devices: {}\n\n'.format(sm.get_devs()))
               message = {'name': 'daemon', 'result': sm.get_devs()}
               time.sleep(1)
            publisher.send_message(1, message)
        except Exception:
            publisher.send_message(1, message)
            continue



if __name__ == "__main__":
    main()

import os
import re
import time
import json
import subprocess
from threading import Thread
import configparser

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.scrolledtext as stk
import tkinter.messagebox as mtk

from settings import config_path, src_path


class StateGUI(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.notebook = ttk.Notebook()
        self.open_config()
        self.add_tab()
        self.notebook.pack(fill=tk.BOTH, expand=1)
        tk.Label(self, text="State Machine panel").pack()
        tk.Button(self, text="Close", command=self.quit).pack(side='right')
        tk.Button(self, text="Save", command=self.save_config).pack(side='right')
        self.run_daemon = tk.Button(self, text="Run daemon", command=self.start_daemon)
        self.run_daemon.pack(side='left')
        self.stop_daemon = tk.Button(self, text="Stop daemon", state='disabled',
                            command=lambda: self.stop_process(self.win1, self.process1, 'daemon'))
        self.stop_daemon.pack(side='left')
        self.run_dev = tk.Button(self, text="Run devs", command=self.start_dev)
        self.run_dev.pack(side='left')
        self.stop_dev = tk.Button(self, text="Stop devs", state='disabled',
                            command=lambda: self.stop_process(self.win2, self.process2, 'dev'))
        self.stop_dev.pack(side='left')

    def add_tab(self, *args, **kwargs):
        self.dev = StateDev(self.notebook, "Append devices", self.device_list)
        self.state = StateDev(self.notebook, "Append states", self.state_list)
        self.matrix = Table(self.notebook, self.state_matrix, self.dev, self.state)
        self.settings = Settings(self.notebook)
        self.notebook.add(self.settings, text="settings")
        self.notebook.add(self.dev, text="dev")
        self.notebook.add(self.state, text="state")
        self.notebook.add(self.matrix, text="matrix")

    def get_list(self, obj):
        return list(map(lambda s: s.split()[0], obj.get(0, tk.END)))

    def save_config(self):
        if not mtk.askokcancel("Save", "Overwrite state machine configuration"):
            return
        self.matrix.draw_matrix()
        with open(os.path.join(config_path, 'data.json'), 'w') as fp:
            data = {
                'matrix': self.matrix.state_matrix,
                'devices': self.dev.listBox.get(0, tk.END),
                'states': self.state.listBox.get(0, tk.END)
            }
            json.dump(data, fp)

    def open_config(self):
        try:
            with open(os.path.join(config_path, 'data.json'), 'r') as fp:
                data = json.load(fp)
            self.state_matrix = data['matrix']
            self.device_list = data['devices']
            self.state_list = data['states']
        except FileNotFoundError:
            self.state_matrix = []
            self.device_list = []
            self.state_list = []

    def start_daemon(self):
        self.win1 = tk.Toplevel(self)
        script = os.path.join(src_path, "daemon.py")
        self.process1 = subprocess.Popen(["python3", script], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.daemon = Thread(target=self.run_window,
                             args=[self.process1, "State machine messages", self.win1, 'daemon'])
        self.daemon.start()
        self.run_daemon['state'] = 'disabled'
        self.stop_daemon['state'] = 'active'


    def start_dev(self):
        self.win2 = tk.Toplevel(self)
        script = os.path.join(src_path, "devs.py")
        self.process2 = subprocess.Popen(["python3", script], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.devs = Thread(target=self.run_window,
                             args=[self.process2, "Emulated devices messages", self.win2, 'dev'])
        self.devs.start()
        self.run_dev['state'] = 'disabled'
        self.stop_dev['state'] = 'active'

    def stop_process(self, win, process, name):
        process.kill()
        win.destroy()
        if name == 'dev':
            self.stop_dev['state'] = 'disabled'
            self.run_dev['state'] = 'active'
        elif name == 'daemon':
            self.stop_daemon['state'] = 'disabled'
            self.run_daemon['state'] = 'active'

    def run_window(self, *args):
        process = args[0]
        title = args[1]
        win = args[2]
        name = args[3]
        win.attributes('-topmost', 'true')
        win.wm_title(title)
        frame = ttk.Frame(win)
        t = tk.Text(frame, width=80, bg="black", fg="green")
        t.pack(side="left", fill="both")
        s = ttk.Scrollbar(frame)
        s.pack(side="right", fill="y")
        s.config(command=t.yview)
        t.config(yscrollcommand=s.set)
        frame.pack()
        win.update_idletasks()

        win.protocol("WM_DELETE_WINDOW", lambda: self.stop_process(win, process, name))
        for path in self.run_process(process):
            if path == b'':
                break
            t.insert(tk.END, path)
            t.see(tk.END)
            t.update_idletasks()

    def run_process(self, process):
        while True:
            line = process.stdout.readline()
            if not line and process.poll() is not None:
                break
            yield line


class StateDev(tk.Frame):
    def __init__(self, name, text, init_list, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        self.label = tk.Label(self, text=text).pack()
        self.name = text
        self.listBox = tk.Listbox(self, selectmode=tk.MULTIPLE)
        for value in init_list:
            self.listBox.insert(tk.END, value)
        self.listBox.pack(side='top', fill=tk.BOTH, expand=1)
        self.add_button = tk.Button(self, text="Add", command=self.append_window).pack(side='left')
        self.remove_button = tk.Button(self, text="Remove", command=self.remove_row).pack(side='left')

    def append_window(self):
        self.add_window = tk.Toplevel(self)
        self.add_window.wm_title("new state/device")
        tk.Label(self.add_window, text="Please use [a-zA-Z0-9_] symbols for names").pack()
        self.info = tk.Label(self.add_window, text="", fg="red")
        self.info.pack()
        self.entry = tk.Entry(self.add_window)
        self.entry.pack()
        self.entry.focus_set()
        tk.Label(self.add_window, text="Description").pack()
        self.comment = stk.ScrolledText(self.add_window, width=35, height=3)
        self.comment.pack()
        tk.Button(self.add_window, text="Append", command=lambda: self.save_row()).pack(side='left')
        tk.Button(self.add_window, text="Close", command=self.add_window.destroy).pack(side='left')

    def remove_row(self):
        items = self.listBox.curselection()
        for pos, i in enumerate(items):
            idx = int(i) - pos
            self.listBox.delete(idx, idx)
        gui.matrix.create_matrix()
        gui.matrix.draw_matrix()

    def save_row(self):
        pattern = re.compile("^\w+$")
        field = self.entry.get()
        description = self.comment.get(1.0, tk.END)
        value = "{} [ {} ]".format(field, description.replace("\n", " "))
        self.entry.focus_set()
        if field in self.get_list(self.listBox):
            self.info["text"] = "Name exist"
        elif pattern.match(field):
            self.listBox.insert(tk.END, value)
            self.info["text"] = ""
            self.comment.delete(1.0, tk.END)
            self.entry.delete(0, tk.END)
        else:
            self.info["text"] = "Not valid name"
        gui.matrix.create_matrix()
        gui.matrix.draw_matrix()

    def get_list(self, obj):
        return list(map(lambda s: s.split()[0], obj.get(0, tk.END)))


class Table(tk.Frame):
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, args[0], **kwargs)
        self.state_matrix = args[1]
        self.state_sequence = {'None': 'Yes', 'Yes': 'No', 'No': 'None'}
        self.table = ttk.Treeview(self)
        self.hscrollbar = ttk.Scrollbar(self)
        self.vscrollbar = ttk.Scrollbar(self)
        self.device = args[2]
        self.state = args[3]
        tk.Button(self, text="Clean", command=self.clean_matrix).pack(side='top')
        tk.Button(self, text="Erase", command=self.erase_matrix).pack(side='bottom')
        tk.Label(self, text="Select item by left mouse button. Change state by right mouse button").pack()
        if len(self.state_matrix) == 0:
            self.state_matrix = [['Devices', ]]
        self.draw_matrix()

    def clean_matrix(self):
        if mtk.askokcancel("Clean", "Set all states to 'None'"):
            self.create_matrix()
            self.draw_matrix()

    def erase_matrix(self):
        if mtk.askokcancel("Erase", "Remove all states and devices'"):
            self.device.listBox.delete(0, tk.END)
            self.state.listBox.delete(0, tk.END)
            self.get_matrix()

    def draw_matrix(self):
        self.draw_table()
        self.table["columns"] = self.state_matrix[0]
        self.table["displaycolumns"] = self.state_matrix[0]
        for head in self.state_matrix[0]:
            self.table.heading(head, text=head, anchor=tk.CENTER)
            self.table.column(head, anchor=tk.CENTER)
        for row in self.state_matrix[1:]:
            self.table.insert('', tk.END, values=tuple(row))
        self.table.pack(expand=tk.YES, fill=tk.BOTH)
        self.table.bind("<Button-3>", self.on_right_click)

    def create_matrix(self):
        devices = gui.get_list(self.device.listBox)
        states = gui.get_list(self.state.listBox)
        template = [None, ] * len(states)
        header = ['Devices', ] + states
        self.state_matrix = [header, ]
        for dev in devices:
            row = [dev, ] + template
            self.state_matrix += [row, ]

    def get_list(self, obj):
        return list(map(lambda s: s.split()[0], obj.get(0, tk.END)))

    def on_right_click(self, event):
        item = self.table.focus()
        column = int(self.table.identify_column(event.x)[1:]) - 1
        row = int(self.table.identify_row(event.y)[1:])
        if column > 0:
            value = self.state_sequence[self.table.set(item, column)]
            self.table.set(item, column, value)
            self.state_matrix[row][column] = value

    def draw_table(self):
        self.table.destroy()
        self.hscrollbar.destroy()
        self.vscrollbar.destroy()
        ttk.Style().configure('.',
                              relief='flat',
                              borderwidth=1,
                              )
        self.table = ttk.Treeview(self, show="headings", selectmode="browse")
        self.hscrollbar = ttk.Scrollbar(self, orient=tk.HORIZONTAL)
        self.vscrollbar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        self.vscrollbar.config(command=self.table.yview)
        self.hscrollbar.config(command=self.table.xview)
        self.vscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.hscrollbar.pack(side=tk.BOTTOM, fill=tk.X)
        self.table.configure(yscrollcommand=self.vscrollbar.set)
        self.table.configure(xscrollcommand=self.hscrollbar.set)
        self.table.tag_configure('odd', background='#E8E8E8')
        self.table.tag_configure('even', background='#DFDFDF')


class Settings(tk.Frame):
    def __init__(self, *args, **kwargs):
        tk.Frame.__init__(self, *args, **kwargs)
        self.config = configparser.ConfigParser()
        tk.Label(self, text="Proxy IP address").pack()
        self.ip = tk.Entry(self)
        self.ip.pack()
        self.ip.focus_set()
        tk.Label(self, text="State machine timeout").pack()
        self.timeout = tk.Entry(self)
        self.timeout.pack()
        self.info = tk.Label(self, text="", fg="red")
        self.info.pack()
        self.open_config()
        tk.Button(self, text="Save settings", command=self.write_config).pack(side='right')

    def open_config(self):
        self.config.read(os.path.join(config_path, 'config.ini'))
        try:
            self.ip.insert(0, self.config['proxy']['ADDRESS'])
        except KeyError:
            pass
        try:
            self.timeout.insert(0, self.config['DEFAULT']['TIMEOUT'])
        except KeyError:
            pass

    def write_config(self):
        pattern = re.compile("^\d+$")
        ip = self.ip.get()
        timeout = self.timeout.get()
        if not self.validIP(ip):
            self.info["text"] = "Invalid IP Address"
            return
        if not pattern.match(timeout):
            self.info["text"] = "Use only digits for timeout"
            return
        self.config['DEFAULT'] = dict(TIMEOUT=timeout)
        self.config['proxy'] = {
            'ADDRESS':  ip,
            'PORT_PUB': 5555,
            'PORT_SUB': 6666,
        }
        if not mtk.askokcancel("Save", "Overwrite settings file"):
            return
        with open(os.path.join(config_path, 'config.ini'), 'w') as configfile:
            self.config.write(configfile)
        self.info["text"] = ""

    def validIP(self, value):
        pattern = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
        if not pattern.match(value):
            return
        valid = 0
        if len(value) >=7 and len(value) <= 15:
            fields = value.split('.')
            if len(fields) < 5:
                for field in fields:
                    iV = int(field)
                    if iV < 0 or iV > 255:
                        valid = 0
                        break
                    else:
                        return True

gui = StateGUI()
gui.geometry("1020x480")
gui.title("Creating state matrix")
gui.mainloop()

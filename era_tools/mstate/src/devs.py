import os
import sys
import time
import random

from helpers import Publisher, Poller

def main():
    result = {False: 'No', True: 'Yes'}
    # getting only topic "1" (from daemon) messages from subscriber
    poller = Poller("1")
    publisher = Publisher()
    i = 0
    while True:
        try:
            income = poller.recv_zipped_pickle()
        except Exception:
            time.sleep(2)
            continue
        names = income['result']
        for name in names:

            # Here must be call of real device action and it`s result
            choice = random.choice([True, False])
            message = {'name': name, 'result': result[choice]}
            sys.stdout.write("i: {}\n".format(i))
            i += 1
            sys.stdout.write("dev {} said: {}\n".format(name, message))
            publisher.send_message(2, message)


if __name__ == "__main__":
    main()

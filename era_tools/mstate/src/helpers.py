import os
import sys
import configparser

import zmq
import zlib
import _pickle as pickle
from threading import Thread

from settings import config_path

class Poller(Thread):
    def __init__(self, topic):
        super().__init__()
        context = zmq.Context()
        self.config = configparser.ConfigParser()
        self.config.read(os.path.join(config_path, 'config.ini'))
        try:
            proxy = self.config['proxy']
        except KeyError:
            sys.stdout.write('PROXY IS NOT CONFIGURED\nUsing 127.0.0.1\n\n')
            proxy = {
                'ADDRESS': '127.0.0.1',
                'PORT_PUB': '5555',
                'PORT_SUB': '5555',
            }

        self.sub = context.socket(zmq.SUB)
        self.sub.setsockopt(zmq.RCVTIMEO, 7000)
        self.sub.connect("tcp://{}:{}".format(proxy['ADDRESS'], proxy['PORT_PUB']))
        self.sub.setsockopt_string(zmq.SUBSCRIBE, topic)

    def recv_zipped_pickle(self, flags=0, protocol=-1):
        [topic, z] = self.sub.recv_multipart(flags)
        p = zlib.decompress(z)
        return pickle.loads(p)


class Publisher(Thread):

    def __init__(self):
        super().__init__()
        context = zmq.Context()
        self.pub = context.socket(zmq.PUB)
        config = configparser.ConfigParser()
        config.read(os.path.join(config_path, 'config.ini'))
        try:
            proxy = config['proxy']
        except KeyError:
            sys.stdout.write('PROXY IS NOT CONFIGURED\nUsing 127.0.0.1\n\n')
            proxy = {
                'ADDRESS': '127.0.0.1',
                'PORT_SUB': '6666',
            }
        self.pub.connect("tcp://{}:{}".format(proxy['ADDRESS'], proxy['PORT_SUB']))

    def send_message(self, topic, obj, flags=0, protocol=-1):
        p = pickle.dumps(obj, protocol)
        z = zlib.compress(p)
        self.pub.send_multipart([b'%d' % topic, z], flags=flags)

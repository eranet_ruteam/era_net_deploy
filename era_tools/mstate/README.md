### Project structure description ###

docker-compose.yml - service named `sm`

## Catalogues ##
* **config** - proxy configuration in `config.ini`, State Machine configuration in `data.json`
* **src** - source python scripts
* **docker-commands** - build and push commands

## Python scripts description ##
* **daemon.py** - State Machine class, ZMQ poller class zmq.SUB which get device data
* **devs.py** - device emulator, send device data by zmq.PUB
* **settings.py** - paths
* **tkConfig.py** - State Machine configuration interface
* **zmqproxy.py** - ZMQ proxy

## State Machine methods ##

zero_state
    """ switch to initial state """

next_state
    """ move to the next state and ignore None results """

check_device_result(message)
    """
    delete successful mach from current_matrix
    :param message: dict.keys('name', 'state', 'result') 
    """
is_ready_to_next
    """ if current_matrix zero-lenght """
 
get_state
    """ current machine state """

check_timeout
    """ time since state was changed """

## ZeroMQ scheme ##
![picture](../../img/zmq.png)

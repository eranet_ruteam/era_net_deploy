from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import statistics

def toxic_points_stat():

    lidar2_robot_tilt_stat = [0.002151509797361932, 0.003722215864510447, -0.00346296188753626, -0.0028194406969125506]
    lidar2_robot_rotation_stat = [-1.78327251997973, -1.7698482993475662, -1.7796506824540574, -1.772133266155555]
    lidar2robot_trans_stat = [[0.5776359465340741, -0.9259849413780421, -2.8538651059629703],
                              [0.9034318177635055, -0.9121562349830341, -3.2051609926234663],
                              [0.5500069100318514, -0.920003994042188, -2.7649245523062644],
                              [0.5787462370499306, -0.9183850095624169, -2.7784885558236785]]
    x = []
    y = []
    z = []


    for line in lidar2robot_trans_stat:
        x.append(line[0])
        y.append(line[1])
        z.append(line[2])

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x, y, z,  c='r', marker='o')

    medx = statistics.median(x)
    medy = statistics.median(y)
    medz = statistics.median(z)
    # if single usage or set
    if 0 < len(lidar2_robot_rotation_stat) < 4:
        file = open("./result/lid2robot2desc.txt", "wt")
        file.write("[DEFAULT]" + "\n\n")
        file.write("rotation_l2r = " + str(lidar2_robot_rotation_stat[0]) + "\n")
        file.write("tilt_l2d = " + str(lidar2_robot_tilt_stat[0]) + "\n")
        file.write("translation_l2r = " + str(lidar2robot_trans_stat[0]) + "\n")
        file.close()
    if len(lidar2_robot_rotation_stat) > 3:
        x = []
        y = []
        z = []

        for line in lidar2robot_trans_stat:
            x.append(line[0])
            y.append(line[1])
            z.append(line[2])

        medx = statistics.median(x)
        medy = statistics.median(y)
        medz = statistics.median(z)
        medrot = statistics.median(lidar2_robot_rotation_stat)
        medtilt = statistics.median(lidar2_robot_tilt_stat)
        print("++++Result of filtering toxic+++++++++++++++")
        print("L2R translation = ", medx, medy, medz)
        print("L2R rotation = ", medrot)
        print("L2R tilt = ", medtilt)
        file = open("./result/lid2robot2desc.txt", "wt")
        file.write("[DEFAULT]" + "\n\n")
        file.write("translation_l2r = " + str([medx, medy, medz]) + "\n")
        file.write("tilt_l2d = " + str(medtilt) + "\n")
        file.write("rotation_l2r = " + str(medrot) + "\n")
        file.close()



    ax.scatter(statistics.median(x), statistics.median(y), statistics.median(z), c='b', marker='x')

    ax.set_xlabel('left-right')
    ax.set_ylabel('up-down')
    ax.set_zlabel('dept')

    plt.show()



#    file = open("./result/lid2robot2desc.txt", "wt")

#    file.write("[DEFAULT]" + "\n\n")
#    file.write("rotation_l2r = " + str(lidar2kucarot_corr) + "\n")
#    file.write("tilt_l2d = " + str(tilt) + "\n")
#    file.write("translation_l2r = " + str([dcx, dcy, dcz]) + "\n")
#    file.close()


if __name__ == "__main__":
    toxic_points_stat()

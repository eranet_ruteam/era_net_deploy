#!/usr/bin/env python3
# -*- coding: UTF-8 -*-


import configparser
import json
import pickle
from tkinter import *
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
import glob
import os

import cv2
import numpy as np
import open3d as lib3d
from PIL import ImageTk, Image
from cv2 import aruco
from scipy import stats
import statistics

def make_camera_matrices():
    global mtx
    global dist
#    dist = np.asarray([0, 0, 0, 0, 0], np.float64)

    cx = 335.21
    cy = 232.354
    fx = 615.973
    fy = 615.584

    mtx = np.asarray( [[1.32057041e+03, 0.00000000e+00, 8.28909765e+02],
        [0.00000000e+00, 1.32057041e+03, 5.78545571e+02],
        [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])

    dist = np.asarray( [1.82758599e+01, -1.02638259e+02, -5.69489890e-04,
             4.57642894e-03, 1.19863563e+02,1.80792479e+01,
            -1.01582634e+02, 1.18452641e+02] )

def process_of_cloudset(mode = 'default'):
    datadir = "./data/fly2/"
    datasets = np.array([datadir + f for f in os.listdir(datadir) if f.endswith(".pcd") ])
#    global pcd_h_read

#    global rotangle
#    (angle_x, angle_y, angle_z, scale) = rotangle
#    global scanning
    scanning = {}
#    global max_zone_radius
#    global defaultimgfile
#    global defaultpcdfile
#    global globalidx
#    global kuka2lidarposangle


    beta = 0

    # Ось x - слева направо
    # Ось y - снизу вверх
    # Ось z -

    flip_transform = np.array([[1000, 0, 0, 0],
                               [0, 0, 1000, 0],
                               [0, -1000, 0, 0],
                               [0, 0, 0, 1]])

#   Цикл по считанным клаудам
    for idx in range(0,len(datasets)):
        fname = datasets[idx]
        pcd_h_read = lib3d.read_point_cloud(fname)
        print("read {} from {}".format(len(pcd_h_read.points), fname))
#   повернули в камера вью
        pcd_h_read.transform(flip_transform)

        num_readed = np.asarray(pcd_h_read.points)
        for (xl, yl, zl) in num_readed:
# to mm с окуглением дл 10мм. Тут следует
            xi = int(round(xl/50.))*50
            yi = int(round(yl/50.))*50
            zi = int(round(zl/50.))*50
#   конструируем индекс работам строго в рамках 3м
            if -3000 < xi < 3000 and -3000 < yi <3000 and -3000 < zi < 3000:
                dictidx = "{:<6d} {:<6d} {:<6d}".format(xi, yi, zi)
                try:
                    value = scanning[dictidx]
                    scanning[dictidx] = value + 1
                except KeyError:
                    scanning[dictidx] = 1
    lidar2 = []
    for k,v in scanning.items():
        if v < 5:
            xx = int(k[0:5])
            yy = int(k[7:12])
            zz = int(k[14:])
            lidar2.append([xx, yy, zz])
            print (k, xx,yy,xx, v)
    lidar_res = lib3d.PointCloud()
    lidar_res.points = lib3d.Vector3dVector(lidar2)
    pcd_tree = lib3d.KDTreeFlann(lidar_res)

    lidar_res.paint_uniform_color([1,0,0])
    pcd_h_read.paint_uniform_color([0,0,1])

    lib3d.draw_geometries([lidar_res])
    print(str(len(scanning)) + " points in dict")
    return



def main():
    cv_version = cv2.__version__
    family = cv_version.split('.')
    if int(family[0]) < 3:
        print("Please install opencv 3.0 and newer. Can't continue")
        return
    process_of_cloudset('default')
    return
#    ReadConfigfile()
    global w1
    master = Tk()
    master.resizable(1280, 500)

    frame1 = Frame(master, relief=RAISED, borderwidth=1)
    frame1.pack(fill=BOTH, expand=True, side=TOP)
#    frame3 = Frame(master, relief=RAISED, borderwidth=1)
#    frame3.pack(fill=BOTH, expand=True, side=TOP)

    frame2 = Frame(master, relief=RAISED, borderwidth=1)
    frame2.pack(fill=BOTH, expand=True, side=TOP)

    Label(frame1, text="rot:").pack(anchor=W, side=LEFT)

    w1 = Canvas(frame2, width=1280, height=1000)
    w1.pack(anchor=W, expand=YES, fill=BOTH, side=LEFT)
    w1.create_line(0, 0, 200, 100)
    w1.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))

    global info_col
    info_col = StringVar()
    info_col.set("Wating")

#    Button(frame1, text="Print_mat", bg="green", command=button_apply2).pack(anchor=W, side=LEFT)
    Label(frame1, textvariable=info_col).pack(anchor=W, side=LEFT)
    Button(frame1, text="GoGO", command=lambda:process_of_cloudset('default'), width=10).pack(anchor=N, side=RIGHT)
    master.mainloop()


if __name__ == "__main__":
    main()

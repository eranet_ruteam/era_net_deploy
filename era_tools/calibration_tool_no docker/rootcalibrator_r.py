#!/usr/bin/env python3
# -*- coding: UTF-8 -*-


import configparser
import json
import pickle
from tkinter import *
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
import glob

import cv2
import numpy as np
import open3d as lib3d
from PIL import ImageTk, Image
from cv2 import aruco
from scipy import stats
import statistics

# global variables definition

rot_step = np.pi / 48.

rotangle = [0, 0, 0, 8]
rotcenter = [0., 0., 0.]
rotAnchor = [0., 0., 0.]

x_width = 1280
y_height = 720
descsize = [0., 0.]  # size of boad in Meters horizontal,vetrical

rotated_cl = []
unrotated_cl = []
scanning = []
marks = []
aruco_position = []
flag_markstate = 0
max_zone_radius = 4
CameraMatrix2 = []
CameraDistorsion = []

info_col = None
global_mode = None

w = None
w1 = None
tkimage = None
img = None
pcd_h_read = None
autoclickpt = None
scale_imgs = 2

rot_corr = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]], np.float64)  # rotation vector zero rotation
trans_corr = np.array([0, 0, 0], np.float64)  # translation vector zero translation
flag_corr_ok = False
check_reproj_var = None
lidar2_robot_rotation_stat = []
lidar2robot_trans_stat = []
lidar2_robot_tilt_stat = []

lid2image_rotation_stat = []
lid2image_translation_stat = []


marks_for_debug = []

# for automatic descboard searching
lm = 0
rm = 0
uc = 0
dc = 0
rot = 0
tilt = 0
dcx = 0
dcy = 0
dcz = 0

# defalut filenames
defaultpcdfile = None
defaultimgfile = None
globalidx = 0


def aruco_process(name):
    global CameraMatrix2
    global CameraDistorsion
    global aruco_position
    aruco_position.clear()
    image = cv2.imread(name)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    aruco_dict = aruco.Dictionary_get(aruco.DICT_ARUCO_ORIGINAL)
    parameters = aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(image, aruco_dict)
    if np.all(len(corners) == 4):
        aruco_position = [[0, 0], [0, 0], [0, 0], [0, 0]]
        #        rvec, tvec, _ = aruco.estimatePoseSingleMarkers(corners[0], 0.05, CameraMatrix2,
        #                                                        CameraDistorsion)  # Estimate pose of each marker and return the values rvet and tvec---different from camera coefficients
        print("ARUCO found %d points" % len(corners))

        xmid = 0
        ymid = 0
        for id, corn in enumerate(corners):
            xmid += corn[0][0][0]
            ymid += corn[0][0][1]
        xmid /= 4
        ymid /= 4
        for id, corn in enumerate(corners):

            if ids[id][0]==771:
                if corn[0][0][0]>xmid:
                    aruco_position[3] = [corn[0][0][0], corn[0][0][1]] # r
                else:
                    aruco_position[0] = [corn[0][2][0], corn[0][2][1]] # d
            if ids[id][0]==0:
                if corn[0][0][1] > ymid:
                    aruco_position[2] = [corn[0][2][0], corn[0][2][1]] # l
                else:
                    aruco_position[1] = [corn[0][0][0], corn[0][0][1]] # u



def distance(pointa, pointb):
    dist = np.sqrt((pointa[0] - pointb[0]) ** 2 + (pointa[1] - pointb[1]) ** 2 + (pointa[2] - pointb[2]) ** 2)
    return dist

#
# ------------------------------------------------------------------------
# Please look here
# Flowchart


def autobox(mode=None, idxclick=0):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    global global_mode
    global scanning
    global pcd_h_read
    global marks
    global autoclickpt
    global descsize
    global rot
    global tilt
    global dcx
    global dcy
    global dcz
    global lidar2_robot_rotation_stat
    global lidar2robot_trans_stat
    global lidar2_robot_tilt_stat
    global marks_for_debug

    marks_for_debug = []
    marks_work = []

    if mode == 1:
        infopanel("please click to box longest line below horisontal diagoal", 1)
        global_mode = 'wait_click'

    cloud = np.delete(np.asarray(scanning, np.float64), 2, 1)
    descboard = []

    xx = np.float64(0.)
    yy = np.float64(0.)
    zz = np.float64(0.)

    if mode == 2 or mode == 3:  # return after click

        pcd = lib3d.PointCloud()
        pcd.points = lib3d.Vector3dVector(cloud)
        pcd_tree = lib3d.KDTreeFlann(pcd)

        minx = 1e9
        maxx = 1e-9
# few worlds pcd PCD. All points after reading got index which could be used to identify point regardless rotation.
# all points inside
# the input data here - idxclick - index of point which was used by operator to identify board by clicking line.
        minid = idxclick
        maxid = idxclick

# we only interesting in points round descsize/2 from click position

        [k, idx, _] = pcd_tree.search_radius_vector_3d(pcd.points[idxclick], descsize[0]/2)
        flagstop = True
        yellowpoints = []
        yellowpoints.append(idxclick)
        infopanel("Please wait", 0)
        while flagstop == True:
            flagstop = False
            for id in idx:
                if scanning[id][3] == 254:
                    continue
                for id2 in yellowpoints:
                    if distance(scanning[id],scanning[id2]) < 0.08:  #0.05m
                        scanning[id][3] = 254
                        yellowpoints.append(id)
                        flagstop = True
                        break


        for id in yellowpoints:
            if scanning[id][0] < minx:
                minid = id
                minx = scanning[id][0]
            if scanning[id][0] > maxx:
                maxid = id
                maxx = scanning[id][0]

# here all points of lidar data clicked line were marked set


#below we will set data from line into arrays for regression. We will calculate middle
                #  point of line to find first coordinate of center board here
        if mode == 2:  # rotation estimation
            shiftd = distance(scanning[minid], scanning[maxid])
            [k, idx, _] = pcd_tree.search_radius_vector_3d(pcd.points[idxclick], descsize[0])
            k = 0
            regrx = []
            regry = []
            regrz = []

            for id in idx:
                if scanning[id][3] == 254:
                    xx += scanning[id][0]
                    yy += scanning[id][1]
                    zz += scanning[id][2]

                    regrx.append(scanning[id][0])
                    regry.append(scanning[id][1])
                    regrz.append(scanning[id][2])
                    k += 1

            xx = xx / k
            yy = yy / k
            zz = zz / k

            # rotation angle estimation
            rot = 0.

            gradient, intercept, r_value, p_value, std_err = stats.linregress(regrx, regrz)
            print("int",intercept,"rval",r_value,"p_val" ,p_value,"str_err",std_err)
            rot = np.arctan(gradient)


            rotangle[1] = - rot
            rotangle[0] = 0
            redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])

# first approximation center of board here
            dcx = xx  # left-right
            dcy = yy + (descsize[0] - shiftd / 2.)+0.15  # forward-back - z ui world
            dcz = zz  # up down - y in world coords

# here dcx,dcy,dcz is draft estimation of center of board and rot it is rotation regarding lidar

        if mode == 2:

            descboard = []
            descdiag = descsize[0]
            descboard.append([scanning[id][0], scanning[id][1], scanning[id][2], id])
            dconners = np.asarray([[0, -descdiag, 0, 0],
                                   [0, descdiag, 0, 0],
                                   [-descdiag, 0, 0, 0],
                                   [descdiag, 0, 0, 0]])

            k = 0
            regrx = []
            regry = []
            regrz = []

            rotate_mat0 = make_transform(0, -rot, 0)  # rotate to face view
            rotate_mat1 = make_transform(0, rot, 0)  # rotate to real view
            k1 = 0
            xx1 = 0
            yy1 = 0
            zz1 = 0
# Идея - повернуть все в портретное состояние - z - координата глубины "от лидара"
# Крутим вектор оценки центра доски
# Key future: to rotate everything into portrait state where  z - depth coordinate "axis from lidar"
# Firstly turning of vector assessment center board

            (dcrx, dcry, dcrz, _) = np.dot([dcx, dcy, dcz, 0], rotate_mat0)
            flatXY = []
            regrx = []
            regry = []
            regrz = []
#All points of cloud near np.sqrt(2) * descdiag *1.2
# 1.2 - (20%) because center of board is currently only is first approximated
            for id  in range(0,len(scanning)):
                if distance(scanning[id], [dcx, dcy, dcz, 0]) < np.sqrt(2) * descdiag * 1.2:
                    (x, y, z, _) = np.dot(scanning[id], rotate_mat0) # крутим всю доску

#Here we are filtering depth of desc cloud for  100mm to improve coordinates and rotation
# x ( horisontal) and z ( depth) could be improved as trivial averaging because there is symmetion for x and z

                    if abs(z - dcrz) < 0.05 and abs(x - dcrx) < (descdiag*1.2 - abs(y - dcry)):
                        scanning[id][3] = 253
                        flatXY.append([x,y])
#accumulation for averaging
                        k1 += 1
                        zz1 += z
                        xx1 += x
                        yy1 += y
                        regrx.append(x)
                        regry.append(y)
                        regrz.append(z)

#коррекция угла поворота по отфильтрованной доске при помощи регрессии
# correction of the angle of rotation on the filtered board using regression
            if len(regrx) > 0:
                gradient, intercept, r_value, p_value, std_err = stats.linregress(regrx, regrz)

                rot_corr = np.arctan(gradient)
                print("Correction:rot",rot_corr," int", intercept, "rval", r_value, "p_val", p_value, "str_err", std_err)

                rot = rot + rot_corr
                rotate_mat1 = make_transform(0, rot, 0)  # rotate to real vieww
# the main problem how to improove y ( vertical)
# We will try to find all end of lines
#          x......................x
#                x...........x
#                    x....x
#


#Отсортировали по y
# here is sorting for y
            ysorted = sorted(flatXY,key=lambda x:x[1])
            beginpointy = ysorted[0][1]
            midy = []
            set_of_x = []
            lines_y = {}
# here separation for lines
            for x,y in ysorted:
                if abs(y-beginpointy) > 0.05:
                    if len(midy)>0:
                        lines_y[sum(midy)/len(midy)] = set_of_x
                    beginpointy = y
                    midy = []
                    set_of_x = []
                else:
                    midy.append(y)
                    set_of_x.append(x)

# цикл по горизонтальным линиям с усреднением координат
# cycle along horizontal lines with averaging coordinates

            dcycc = []
            dcyccol = 0

            for yy,list_of_x in lines_y.items():

                maxx = max(list_of_x)
                minx = min(list_of_x)


#коррекция положения доски по y
#correction y with using any line of descboards
                if minx - maxx < descdiag:
                    marks_work.append([maxx, yy, zz1 / k1, 0])
                    marks_work.append([minx, yy, zz1 / k1, 0])
                    if yy > dcry:
                        dcycc.append(yy - (descsize[0] - (maxx - minx) / 2.))
                        dcyccol += 1
                    else:
                        dcycc.append( yy + (descsize[0] - (maxx - minx) / 2.))
                        dcyccol += 1

# Result of correction
# Median filter of each data
            if len(dcycc)>4:
                dcyccs = statistics.median(dcycc)
                dcy = dcyccs
                print("OK vert correction ",dcyccol,"lines lidar used: ", dcycc,dcry)
            else:
                dcycc = dcry
                print ("Fail vert correction")

# Rotaton coordinates back
            (dccrx, dccry, dccrz, _) = np.dot([xx1 / k1, dcyccs, zz1 / k1, 0], rotate_mat1)  # rotate back
            print("->",dcx,dccrx,dcy,dccry,dcz,dccrz)
            dcx = dccrx
            dcz = dccrz
            for x2,y2,z2,i2 in marks_work:
                (x, y, z, _) = np.dot([x2,y2,z2,i2], rotate_mat1)  # rotate back
                marks_for_debug.append([x, y, z])

# tilt estimation
            gradient2, intercept2, r_value2, p_value2, std_err2 = stats.linregress(regry, regrz)
            tilt = -np.arctan(gradient2)
            rotate_mat2 = make_transform(tilt, 0, 0)
# store corners of board into markers array where alredy Aruco or not
            for idx, value in enumerate(dconners):
                line = np.dot(value, rotate_mat2)
                rotated = np.dot(line, rotate_mat1)
                xc = dcx + rotated[0]
                yc = dcy + rotated[1]
                zc = dcz + rotated[2]
# if aruco markers did not detectred
                if len(aruco_position) != 4:
                    marks.append([-idx - 1, 0, [xc, yc, zc, 0], None, None])  # DC
# if aruco markers is OK
                else:
                    marks.append([-idx - 1, 0, [xc, yc, zc, 0], aruco_position[idx][0], aruco_position[idx][1]])  # DC


            rotangle[0] = -tilt;
            lidar2_robot_tilt_stat.append(tilt)
            redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
            redraw_image()
# If automatical mode with aruco
            if len(aruco_position) != 4:
                infopanel("please follow to image corresponding green markers", str(1))
                global_mode = 'wait_click_4img'
                info_col.set("Please select 4 point on camera panel")
                autoclickpt = -4
# if without aruco - handmade mode
            else:
                autoclickpt = 0
                global_mode = None


# Сохраним рассчитанй угол поворота систем координат робота и лидара односительно друг друга
# lidar2_robot_rotation_stat список, который будет хранить значения вычисленных разниц углов между системами координат робота и лидара
            lidar2_robot_rotation_stat.append(-rot - kuka2lidarposangle[3])


# посчитаем координату робота в координатах лидара для текущего угла
# и положим ее lidar2robot_trans_stat
# calculation of the coordinate of the robot in the coordinates of the lidar for the current angle
# and put it into lidar2robot_trans_stat
            robot_zero_shift = [kuka2lidarposangle[0], kuka2lidarposangle[1], kuka2lidarposangle[2]] # x, y, z
            transform2 = make_transform3x3(0, -(-rot - kuka2lidarposangle[3]), 0)
            (x, y, z) = np.dot(np.asarray(robot_zero_shift), transform2)
            x0 = dccrx - x
            y0 = dccry - y
            z0 = dccrz + z

            lidar2robot_trans_stat.append([x0,y0,z0])

#контрольный принт
#control print
            print("__________")
            print("current robot position",robot_zero_shift)
            print("current from lidar to desc", [dccrx,dccry,dccrz])
            print("current shift-robot-lidar-in-lidar(i): [%f,%f,%f]" % (x0, y0, z0))
            print("current robot2-lidar rotation ", -rot - kuka2lidarposangle[3])
            print("__________")
# усреднене будет сделано по кнопке "apply" в функции "toxic_ponts_stat"
#    averaged   will    be    done    by    the    "apply"    button in the    apply2    function
    return


# function for filtering position robot in lidar coord
def toxic_points_stat():
    global lidar2_robot_tilt_stat
    global lidar2_robot_rotation_stat
    global lidar2robot_trans_stat
    global marks

    global lid2image_rotation_stat
    global lid2image_translation_stat

    x = []
    y = []
    z = []
    print("tilt++++++++++++++++++")
    print (lidar2_robot_tilt_stat)
    print("")
    print("rot_++++++++++++++++++")
    print(lidar2_robot_rotation_stat)
    print()
    print("trans_++++++++++++++++++")
    print(lidar2robot_trans_stat )


    for line in lidar2robot_trans_stat:
        x.append(line[0])
        y.append(line[1])
        z.append(line[2])
    # if single usage or set
    if 0 < len(lidar2_robot_rotation_stat) < 4:
        file = open("./result/lid2robot2desc.txt", "wt")
        file.write("[DEFAULT]" + "\n\n")
        file.write("rotation_l2r = " + str(lidar2_robot_rotation_stat[0]) + "\n")
        file.write("tilt_l2d = " + str(lidar2_robot_tilt_stat[0]) + "\n")
        file.write("translation_l2r = " + str(lidar2robot_trans_stat[0]) + "\n")
        file.close()

    if len(lidar2_robot_rotation_stat) > 3:
        x = []
        y = []
        z = []

        for line in lidar2robot_trans_stat:
            x.append(line[0])
            y.append(line[1])
            z.append(line[2])

        medx = statistics.median(x)
        medy = statistics.median(y)
        medz = statistics.median(z)
        medrot = statistics.median(lidar2_robot_rotation_stat)
        medtilt = statistics.median(lidar2_robot_tilt_stat)
        print("++++Result of filtering toxic lidar2r+++++++++++++++")
        print("L2R translation = ", medx, medy, medz)
        print("L2R rotation = ", medrot)
        print("L2R tilt = ", medtilt)
        file = open("./result/lid2robot2desc.txt", "wt")
        file.write("[DEFAULT]" + "\n\n")
        file.write("translation_l2r = " + str([medx, medy, medz]) + "\n")
        file.write("tilt_l2d = " + str(medtilt) + "\n")
        file.write("rotation_l2r = " + str(medrot) + "\n")
        file.close()


    return

def lid2image_median_correction():
    return
    global lid2image_rotation_stat
    global lid2image_translation_stat
    global rot_corr
    global trans_corr
    print("___________trans data for correction___",lid2image_translation_stat)
    print("___________rot data for correction___",lid2image_rotation_stat)

    if len(lid2image_rotation_stat) < 4:
        print("No correction( {} positions - 4 min)".format(len(lid2image_rotation_stat)))
        return
    x = []
    y = []
    z = []

    a = []
    b = []
    c = []


    for idx in range(0,len(lid2image_rotation_stat)):
        a.append(lid2image_rotation_stat[idx][0])
        b.append(lid2image_rotation_stat[idx][1])
        c.append(lid2image_rotation_stat[idx][2])
        x.append(lid2image_translation_stat[idx][0])
        y.append(lid2image_translation_stat[idx][1])
        z.append(lid2image_translation_stat[idx][2])
    rot_corr=np.asarray([[statistics.median(a),statistics.median(b),statistics.median(c)]])
    trans_corr = np.asarray([[statistics.median(x),statistics.median(y),statistics.median(z)]])
    print("after correction->",rot_corr,trans_corr)



# main calibration function
def button_apply2():
    points_3d = []
    points_2d = []

    global rot_corr
    global trans_corr
    global marks
    global flag_corr_ok
    global rotangle
    global CameraMatrix2
    global CameraDistorsion
    global lid2image_rotation_stat
    global lid2image_translation_stat


    gamma = np.pi / 4  # rotation will be calculated from -x radar axis
    rotation_z = make_transform(0, 0, gamma)

    #toxic points filration
    toxic_points_stat()

    for lines in marks:
        (xz, yz, zz, intensiv) = lines[2]
        line = np.array([xz, yz, zz, intensiv])
        rotated = np.dot(line, rotation_z)

        (x, y, z, i_light) = rotated
        xp, yp = (lines[3], lines[4])
        mtx = np.asarray(CameraMatrix2, np.float64)
        dist = np.asarray(CameraDistorsion, np.float64)
        # append points
        points_3d.append([-x, z, y])  # pattern points
        points_2d.append([xp, yp])  # camera points

    objpoints = np.asarray(points_3d)  # (xyz) points of object
    objpoints = objpoints.astype('float64')
    imgpoints = np.asarray(points_2d)  # lidar = left camera
    imgpoints = imgpoints.astype('float64')

    mtx = np.asarray(CameraMatrix2)  # CameraMatrix
    dist = np.asarray(CameraDistorsion)  # # distorsion matrix
#    if len(imgpoints)>8:
#        print("solveRansacUsed")
#    _ret, rot_corr, trans_corr, inliers = cv2.solvePnPRansac(objpoints, imgpoints, mtx, dist)
#       else:
#       print("solvePNPUsed")
    _ret, rot_corr, trans_corr = cv2.solvePnP(objpoints, imgpoints, cameraMatrix=mtx, distCoeffs=dist, flags=cv2.SOLVEPNP_ITERATIVE  )
    print(_ret)
    _ret = True
    if _ret == True:

        lid2image_rotation_stat.append([rot_corr[0][0],rot_corr[1][0],rot_corr[2][0]])
        lid2image_translation_stat.append([trans_corr[0][0],trans_corr[1][0],trans_corr[2][0]])

        flag_corr_ok = True
        print("ROTATION_Lid2Image:")
        print(rot_corr)
        file = open("./result/rotation_lid2image.txt", "wt")
        file.write(str(rot_corr))
        file.close()

        print("TRANSLATION_lid2Image:")
        print(trans_corr)
        file = open("./result/translation_lid2image.txt", "wt")
        file.write(str(trans_corr))
        file.close()
    lid2image_median_correction()
    redraw_image()
# Here there is a section for estimation rotation and translation robot 2 lidar coord and back
# We have set of lidar-robot rotation and lidar-robot translation corespondsenses
# We should find and filter "toxic" ponts



    return


def infopanel(message, type="error"):
    if type == 'error':
        messagebox.showerror("Error", message)

    else:
        messagebox.showinfo("Information", message)

    return


def button_rem_mark(mode=0):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    global marks
    global flag_markstate

    if mode == 0:
        marks.clear()
        flag_markstate = 0
    if mode == 1:
        if len(marks) > 0:
            marks.pop()
        flag_markstate = 0

    redraw_pcd(angle_x, angle_y, angle_z, scale)
    redraw_image()

    return


def button_saverestoremark(mode=0):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    global marks
    global flag_markstate

    if mode == 0:  # load_marks
        marks.clear()
        try:
            with open('./data/marks.dat', 'rb') as fmarks:
                marks = pickle.load(fmarks)
        except FileNotFoundError:
            infopanel("Error filereading marks", "error")

            # print(marks)

    if mode == 1:  # save_marks

        if flag_markstate != 0:
            infopanel("Can't save marks, please finish pair click", "error")
            return

        fmarks = open('./data/marks.dat', 'wb')
        pickle.dump(marks, fmarks, protocol=pickle.HIGHEST_PROTOCOL)
        fmarks.close()

    redraw_pcd(angle_x, angle_y, angle_z, scale)
    redraw_image()

    return


def click_on_image(event):
    global info_col
    global flag_markstate
    global marks
    global global_mode
    global autoclickpt
    global rotangle

    global lm
    global rm
    global uc
    global dc
    global descsize
    global rot
    global tilt
    global dcx
    global dcy
    global dcz
    global scale_imgs

    (angle_x, angle_y, angle_z, scale) = rotangle

    if global_mode == 'wait_click_4img':
        for idx in range(0,len(marks)):
            if marks[idx][0] == autoclickpt:

                if autoclickpt == -4:
                    lm = idx
                elif autoclickpt == -3:
                    rm = idx
                elif autoclickpt == -2:
                    uc = idx
                elif autoclickpt == -1:
                    dc = idx
                marks[idx][3] = event.x * scale_imgs
                marks[idx][4] = event.y * scale_imgs
                marks[idx][0] = 0
                autoclickpt += 1

                redraw_pcd(angle_x, angle_y, angle_z, scale)

                #                      -2
                #                    xuc yuc
                #        -4 xlm,ylm            xrm,yrm -3
                #                     xdc ydc
                #                       -1
                #

                if autoclickpt == 0:
                    autoclickpt = None
                    global_mode = None

                    xlm = marks[lm][3]
                    ylm = marks[lm][4]

                    xrm = marks[rm][3]
                    yrm = marks[rm][4]

                    xuc = marks[uc][3]
                    yuc = marks[uc][4]

                    xdc = marks[dc][3]
                    ydc = marks[dc][4]

                    #                    marks.append([0, 0, [dcx, dcy, dcz, 0], (xuc + xdc) / 2., (ylm + yrm) / 2.])

                    #                    for ppt in range(1, 5):
                    #                        marks.append(
                    #                            [0, 0,
                    #                             [dcx - descsize[1] * np.sin(rot) / 5. * ppt, dcy + descsize[0] * np.cos(rot) / 5. * ppt,
                    #                              dcz, 0],
                    #                             (xuc + xdc) / 2. + (xrm - xlm) / 10. * ppt, (ylm + yrm) / 2. + (yrm - ylm) / 10. * ppt])
                    #                        marks.append(
                    #                            [0, 0,
                    #                             [dcx + descsize[1] * np.sin(rot) / 5. * ppt, dcy - descsize[0] * np.cos(rot) / 5. * ppt,
                    #                              dcz,
                    #                              0], (xuc + xdc) / 2. - (xrm - xlm) / 10. * ppt,
                    #                             (ylm + yrm) / 2. - (yrm - ylm) / 10. * ppt])

                    #                        marks.append([0, 0,
                    #                                      [dcx ,
                    #                                       dcy,
                    #                                       dcz - descsize[0] / 5. * ppt, 0],
                    #                                      (xuc + xdc) / 2. - (xuc - xdc) / 10. * ppt,
                    #                                      (ylm + yrm) / 2. + (ydc - yuc) / 10. * ppt])
                    #
                    #                        marks.append([0, 0,
                    #                                      [dcx,
                    #                                       dcy,
                    #                                       dcz + descsize[0] / 5. * ppt, 0],
                    #                                      (xuc + xdc) / 2. + (xuc - xdc) / 10. * ppt,
                    #                                      (ylm + yrm) / 2. - (ydc - yuc) / 10. * ppt])

                    #                        marks.append([0, 0, [dcx, dcy , dcz - descsize[1] / 5. * ppt, 0],
                    #                                      (xuc + xdc) / 2. - (xuc - xdc) / 10. * ppt,
                    #                                      (ylm + yrm) / 2. + (ydc - yuc) / 10. * ppt])
                    #                        marks.append(
                    #                            [0, 0, [dcx, dcy , dcz + descsize[1] / 5. * ppt, 0],
                    #                             (xuc + xdc) / 2. + (xuc - xdc) / 10. * ppt,
                    #                             (ylm + yrm) / 2. - (ydc - yuc) / 10. * ppt])

                    # marks.append(
                    #                            [0, 0,
                    #                             [dcx + descsize[1] * np.sin(rot) / 5. * ppt, dcy - descsize[0] * np.cos(rot) / 5. * ppt,
                    #                              dcz, 0],
                    #                             (ylm + yrm) / 2. + , (ylm+yrm)/2. - ])

                    redraw_pcd(angle_x, angle_y, angle_z, scale)
                    redraw_image()

                break

    else:
        if flag_markstate == 0:
            info_col.set("Please select point on lidar panel first")
            return
        info_col.set("Please select pair point on image panel")
        marks[-1][3] = event.x * scale_imgs
        marks[-1][4] = event.y * scale_imgs
        flag_markstate = 0
        info_col.set("Please select next point on lidar panel")
    redraw_image()


def make_transform(alpha, beta, gamma, Dx=0., Dy=0., Dz=0.):
    flip_transform_x = [[1, 0, 0, 0],
                        [0, np.cos(alpha), -np.sin(alpha), 0],
                        [0, np.sin(alpha), np.cos(alpha), 0],
                        [0, 0, 0, 1]]

    flip_transform_y = [[np.cos(beta), 0, np.sin(beta), 0],
                        [0, 1, 0, 0],
                        [-np.sin(beta), 0, np.cos(beta), 0],
                        [0, 0, 0, 1]]

    flip_transform_z = [[np.cos(gamma), -np.sin(gamma), 0, 0],
                        [np.sin(gamma), np.cos(gamma), 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1]]

    flip_tot = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    flip_tot = np.dot(flip_tot, flip_transform_y)
    flip_tot = np.dot(flip_tot, flip_transform_x)
    flip_tot = np.dot(flip_tot, flip_transform_z)
    #    flip_tot = np.dot(flip_tot, flip_trans)
    #    print(alpha,beta,gamma)
    return flip_tot

def make_transform3x3(alpha, beta, gamma, Dx=0., Dy=0., Dz=0.):
    flip_transform_x = [[1, 0, 0],
                        [0, np.cos(alpha), -np.sin(alpha)],
                        [0, np.sin(alpha), np.cos(alpha)]]

    flip_transform_y = [[np.cos(beta), 0, np.sin(beta)],
                        [0, 1, 0],
                        [-np.sin(beta), 0, np.cos(beta)]]

    flip_transform_z = [[np.cos(gamma), -np.sin(gamma), 0],
                        [np.sin(gamma), np.cos(gamma), 0],
                        [0, 0, 1]]

    flip_tot = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]

    flip_tot = np.dot(flip_tot, flip_transform_y)
    flip_tot = np.dot(flip_tot, flip_transform_x)
    flip_tot = np.dot(flip_tot, flip_transform_z)

    return flip_tot


def redraw_pcd(alpha, beta, gamma, scale):
    global pcd_h_read
    global tkimage
    global rotated_cl
    global unrotated_cl
    global info_col
    global scanning
    global autoclickpt
    global rotcenter
    global marks_for_debug

    colors = [(255, 255, 255), (255, 0, 255), (0, 255, 255), (255, 255, 0), (255, 0, 0), (127, 0, 0)]

    blank_image = np.zeros((y_height, x_width, 3), np.uint8)

    rotated_cl = []
    unrotated_cl = []
    (Dx, Dy, Dz) = rotcenter

    rotation_tot = make_transform(alpha, beta, gamma, Dx, Dy, Dz)

    for idx in range(0,len(scanning)):
        trio = scanning[idx]
        line = np.array([trio[0], trio[1], trio[2], trio[3]])
        rotated = np.dot(line, rotation_tot)

        (x, y, z, i_light) = rotated

        yz = int(400 / 2 - 800 * y / scale / 2)
        xz = int(800 * x / scale / 2 + 800 / 2)

        if xz <= 0:
            continue
        if yz <= 0:
            continue

        if xz >= x_width:
            continue
        if yz >= y_height:
            continue

        if z < 8:
            blank_image[yz][xz] = (255 * (8 - np.abs(z)) / 7, 255 * (8 - np.abs(z)) / 7, 255)

        if i_light == 255:
            blank_image[yz][xz] = (255, 0, 0)
        elif i_light == 254:
            blank_image[yz][xz] = (255, 255, 0)
        elif i_light == 253:
            blank_image[yz][xz] = (255, 127, 0)

        # закинули в словарики для быстрого анализа клика
        rotated_cl.append((xz, yz, z))
        unrotated_cl.append(idx)

    pclimg = Image.fromarray(blank_image)
    tkimage = ImageTk.PhotoImage(image=pclimg)
    w.delete(all)
    w.create_image(0, 0, image=tkimage, anchor="nw")
    w.update()

    # draw points found

    for lines in marks:
        (xz, yz, zz, intensiv) = lines[2]
        line = np.array([xz, yz, zz, intensiv])
        rotated = np.dot(line, rotation_tot)
        (x, y, z, i_light) = rotated

        #        if z <= 0:
        #            continue  # I haven't eye on the nape


        yz = int(400 / 2 - 800 * y / scale / 2)
        xz = int(800 * x / scale / 2 + 800 / 2)

        if xz <= 0:
            continue
        if yz <= 0:
            continue

        if xz >= x_width:
            continue
        if yz >= y_height:
            continue
        if lines[0] == autoclickpt:
            w.create_oval(xz - 5, yz - 5, xz + 5, yz + 5, fill="#00FF00")
        else:
            w.create_oval(xz - 5, yz - 5, xz + 5, yz + 5, fill="#FF6042")
    # draw status
    if global_mode == 'wait_click_4img':
        info_col.set("Please select pair point on camera panel (marked green)")
    else:
        if flag_markstate == 0:
            info_col.set("Please select point on lidar panel")
        if flag_markstate == 1:
            info_col.set("Please select point on camera panel")

            # draw points found
    for xz,yz,zz in marks_for_debug:

        line = np.array([xz, yz, zz, 0])
        rotated = np.dot(line, rotation_tot)
        (x, y, z, i_light) = rotated

        #        if z <= 0:
        #            continue  # I haven't eye on the nape


        yz = int(400 / 2 - 800 * y / scale / 2)
        xz = int(800 * x / scale / 2 + 800 / 2)
        w.create_oval(xz - 2, yz - 2, xz + 2, yz + 2, fill="#00FFFF")

    return


def trans_move(a, b, c, d):
    global trans_corr
    global marks
    global rotangle
    global global_mode
    global tilt

    if global_mode != 'wait_click_4img':
        infopanel("Actual only when Autobox mode", str(0))
        return

    (angle_x, angle_y, angle_z, scale) = rotangle

    for idx in range(0,len(marks)):
        if marks[idx][0] < 0:
            # print(marks[idx][2])
            marks[idx][2][0] += 0.05 * a
            marks[idx][2][1] += 0.05 * b
            marks[idx][2][2] += 0.05 * c
    redraw_pcd(angle_x, angle_y, angle_z, scale)


def redraw_image():
    global w1
    global marks
    global CameraMatrix2
    global rotangle

    global x_width
    global y_height

    global rot_corr
    global trans_corr
    global tvec
    global flag_corr_ok
    global scanning
    global rotangle
    global check_reproj_var
    global aruco_position
    global scale_imgs

    # (alpha,beta,gamma,scale) = rotangle
    gamma = np.pi / 4.
    rotation_z = make_transform(0, 0, gamma)
    # rotation_x =  make_transform(alpha, 0, 0)
    # rotation_y = make_transform(0,beta, 0)

    w1.delete("all")
    w1.create_image(0, 0, image=img, anchor="nw")
    w1.update()

    mtx = np.asarray(CameraMatrix2, np.float64)
    dist = np.asarray(CameraDistorsion, np.float64)

    for lines in marks:
        (xz, yz, zz, intensiv) = lines[2]
        line = np.array([xz, yz, zz, intensiv])
        rotated = np.dot(line, rotation_z)
        (x, y, z, i_light) = rotated
        xp, yp = (lines[3], lines[4])
        if xp == None or yp == None:
            continue
        xp = xp / scale_imgs
        yp = yp / scale_imgs
        w1.create_oval(xp - 5, yp - 5, xp + 5, yp + 5, fill="#FF6042")

        coordstr = np.asarray([[-x, z, y]], np.float64)

        result1 = cv2.projectPoints(coordstr,
                                    rot_corr,
                                    trans_corr,
                                    mtx,
                                    dist)

        (xx, yy) = result1[0][0][0]
        xx = xx / scale_imgs
        yy = yy / scale_imgs

        w1.create_oval(xx - 5, yy - 5, xx + 5, yy + 5, fill="#00FF00")

    if flag_corr_ok == True:
        for idx in range(0,len(scanning)):
            trio = scanning[idx]
            line = np.array([trio[0], trio[1], trio[2], trio[3]])
            rotated = np.dot(line, rotation_z)
            (x, y, z, i_light) = rotated
            coordstr = np.asarray([[-x, z, y]], np.float64)

            result1 = cv2.projectPoints(coordstr,
                                        rot_corr,
                                        trans_corr,
                                        mtx,
                                        dist)
            (xx, yy) = result1[0][0][0]
            xx = xx / scale_imgs
            yy = yy / scale_imgs

            if 0 < xx < x_width and 0 < yy < y_height and y < 0 and check_reproj_var.get() == 1:
                w1.create_oval(xx - 1, yy - 1, xx + 1, yy + 1, width=1, outline='blue')

    for lines in aruco_position:
        xx = lines[0] / scale_imgs
        yy = lines[1] / scale_imgs
        w1.create_oval(xx - 1, yy - 1, xx + 1, yy + 1, width=1, outline='red')



def load_fileJPG(mode=1,fname = None):
    if mode == 1:
        fname = askopenfilename(filetypes=(("PNG files", "*.png"), ("JPG files", "*.jpg"),
                                           ("All files", "*.*")))
    elif mode == 0:
        fname = defaultimgfile
    # print(fname)
    global img
    global w1
    global scale_imgs

    sourceimg = Image.open(fname)
    [imageSizeWidth, imageSizeHeight] = sourceimg.size
    tag = sourceimg.resize((int(imageSizeWidth / scale_imgs), int(imageSizeHeight / scale_imgs)), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(tag)
    w1.create_image(0, 0, image=img, anchor="nw")
    aruco_process(fname)
    w1.update()
    redraw_image()


def button_rot_left():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_y > -np.pi:
        rotangle[1] -= rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_up():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_x < np.pi:
        rotangle[0] += rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_down():
    """rot down"""
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_x > -np.pi:
        rotangle[0] -= rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_right():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_y < np.pi:
        rotangle[1] += rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_scale_inc():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle
    if scale < 1:
        scale *= 2

    elif scale < 20:
        rotangle[3] += 1
    redraw_pcd(angle_x, angle_y, angle_z, scale)
    return


def button_rot_z(direction):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if abs(angle_z) <= np.pi:
        rotangle[2] += int(direction) * rot_step / 2.
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_scale_dec():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if scale > 1:
        rotangle[3] -= 1
    else:
        rotangle[3] /= 2.

    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])


def button_clear(mode):
    global rotangle

    if mode == 0:
        rotangle = [-0.5235987755982988, 0, 0, 6]
        redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])

    if mode == 1:
        rotangle = [np.pi / 2., 0, 0, 9]
        redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])


def gen_test_pcl():
    global pcd_h_read
    global rotangle
    global scanning

    (angle_x, angle_y, angle_z, scale) = rotangle
    scanning.clear()

    for x in range(-20, 20, 4):
        for y in range(-20, 20, 4):
            for z in range(0, 2, 1):
                if x == 0:
                    scanning.append([x / 5., y / 5., z / 8., 1])
                elif y == 0:
                    scanning.append([x / 5., y / 5., z / 8., 2])
                else:
                    scanning.append([x / 5., y / 5., z / 8., 0])
    redraw_pcd(angle_x, angle_y, angle_z, scale)


def load_filePCD(mode=1):
    global pcd_h_read

    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle
    global scanning
    global max_zone_radius
    global defaultimgfile
    global defaultpcdfile
    global globalidx
    global kuka2lidarposangle

    scanning.clear()

    beta = 0

    # Ось x - слева направо
    # Ось y - снизу вверх
    # Ось z -

    flip_transform = np.array([[1, 0, 0, 0],
                               [0, 0, 1, 0],
                               [0, -1, 0, 0],
                               [0, 0, 0, 1]])
    if mode == 1:
        fname = askopenfilename(filetypes=(("PCD", "*.pcd"), ("All files", "*.*")))
    elif mode == 0:
        fname = defaultpcdfile
        load_fileJPG(0)
        button_saverestoremark(0)
    elif mode == 3:
#        button_rem_mark(0)
        names = glob.glob("./data/fly2/*.pcd")

        onlyname = names[globalidx]
        fname = names[globalidx]
        fnameimg = "%s.png" % onlyname[:-4]
        fnameposition = "%s.txt" % onlyname[:-4]
        load_fileJPG(3,fnameimg)
        kuka2lidarposangle = kukacenter(fnameposition)
        print("load files %s %s %s" % (fnameimg, fname, fnameposition))

        if globalidx < len(names) - 1:
            globalidx += 1
        else:
            infopanel("end of filelist","")

#        button_saverestoremark(0)

    pcd_h_read = lib3d.read_point_cloud(fname)

    pcd_h_read.transform(flip_transform)

    num_readed = np.asarray(pcd_h_read.points)
    for (xl, yl, zl) in num_readed:
        scanning.append([xl, yl, zl, 1])

    print(str(len(scanning)) + " points read")
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def click_analyze(event):
    """
    trying to find something near mouse click

    :param event:
    """
    # координаты клика
    global rotated_cl
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle
    global marks
    global flag_markstate
    global info_col
    global global_mode

    if flag_markstate == 1 and global_mode == None:
        info_col.set("Please select pair point on image panel, lidar panel loked!")
        return

    delta = 2  # area of search delta x delta points

    x, y = (event.x), (event.y)

    # trying to find something testy
    for idx, (sx, sy, sz) in enumerate(rotated_cl):

        if abs(x - sx) < delta and abs(y - sy) < delta:
            position = unrotated_cl[idx]
            if global_mode == None:
                scanning[position][3] = 2  # mark points
                marks.append([x, y, scanning[position], None, None])

                flag_markstate = 1
                info_col.set("Please select pair point on image panel")
            if global_mode == "wait_click2":
                autobox(3, position)
            if global_mode == 'wait_click':
                autobox(2, position)

            break
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def ReadConfigfile():
    global CameraMatrix2
    global CameraDistorsion
    global descsize
    global defaultpcdfile
    global defaultimgfile

    config = configparser.ConfigParser()
    config.read('./config/config.ini')

    # CameraMatrix2 = json.loads(config['DEFAULT']['CameraMatrix'])
    defaultimgfile = config['DEFAULT']['DefaultImgFile']
    defaultpcdfile = config['DEFAULT']['DefaultPcdFile']
    descsize = json.loads(config['DEFAULT']['CalibrationDescSize'])
    descsize[0] = descsize[0] / 2.
    descsize[1] = descsize[1] / 2.
    config.clear()
    config.read('./config/SN1010.conf')
    section = 'RIGHT_CAM_FHD'
    fx = float(config[section]['fx'])
    fy = float(config[section]['fy'])
    cx = float(config[section]['cx'])
    cy = float(config[section]['cy'])
    k1 = float(config[section]['k1'])
    k2 = float(config[section]['k2'])
    p1 = float(config[section]['p1'])
    p2 = float(config[section]['p2'])

    CameraMatrix2 = [[fx, 0, cx], [0, fy, cy], [0, 0, 1]]
    CameraDistorsion = [k1, k2, p1, p2, 0]

    print("ReadConfigBegin")
    print("DISTORSION", CameraDistorsion)
    print("Matrix", CameraMatrix2)
    print("Desc", descsize)
    print("ReadConfigEnd")
    return


def kukacenter(configfile = 'config/descposition.ini'):
    global descsize
    global kukamarks

    if 1 == 1:  # center definition
        config = configparser.ConfigParser()
        config.read(configfile)

        x = json.loads(config['DEFAULT']['x'])
        y = json.loads(config['DEFAULT']['y'])
        z = json.loads(config['DEFAULT']['z'])
        a = json.loads(config['DEFAULT']['a'])
        b = json.loads(config['DEFAULT']['b'])
        c = json.loads(config['DEFAULT']['c'])

#       robot 2 lidar coord system - change axis and mm to meters
        transformation_kz = np.array([
            [0, 0, 0.001, 0],
            [0.001, 0, 0, 0],
            [0, .001, 0, 0],
            [0, 0, 0, 1]])

        (cx, cy, cz, _) = np.dot(np.asarray([x, y, z, 0]), transformation_kz)

        rot = a / 180. * np.pi
        tilt = 0.

        return [cx, cy, cz, rot, tilt]


def main():
    cv_version = cv2.__version__
    family = cv_version.split('.')
    if int(family[0]) < 3:
        print("Please install opencv 3.0 and newer. Can't continue")
        return
    ReadConfigfile()
    kukacenter()

    master = Tk()
    master.resizable(1280, 720)

    global w
    global w1
    global check_reproj_var

    check_reproj_var = IntVar()
    check_reproj_var.set(1)

    frame1 = Frame(master, relief=RAISED, borderwidth=1)
    frame1.pack(fill=BOTH, expand=True, side=TOP)
    frame3 = Frame(master, relief=RAISED, borderwidth=1)
    frame3.pack(fill=BOTH, expand=True, side=TOP)

    frame2 = Frame(master, relief=RAISED, borderwidth=1)
    frame2.pack(fill=BOTH, expand=True, side=TOP)

    Label(frame1, text="Lidar viw control:").pack(anchor=W, side=LEFT)
    Button(frame1, text="<-", bg="blue", fg='white', command=button_rot_left, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="^", bg="blue", fg='white', command=button_rot_up, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="v", bg="blue", fg='white', command=button_rot_down, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="->", bg="blue", fg='white', command=button_rot_right, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="Rot R", bg="blue", fg='white', command=lambda: button_rot_z(1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="Rot L", bg="blue", fg='white', command=lambda: button_rot_z(-1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)

    Button(frame1, text="-", bg="green", fg='white', command=button_scale_inc, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="+", bg="green", fg='white', command=button_scale_dec, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="Top View", bg="red", fg='white', command=lambda: button_clear(1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="DEPT_V", bg="red", fg='white', command=lambda: button_clear(0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="AutoBox", bg="yellow", fg='black', command=lambda: autobox(1)).pack(anchor=W, side=LEFT)

    w = Canvas(frame2, width=400, height=300)
    w.pack(expand=YES, fill=BOTH, side=TOP)
    w.create_line(0, 0, 200, 100)
    w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
    # w.create_rectangle(50, 25, 150, 75, fill="blue")
    w.bind("<Button-1>", click_analyze)

    w1 = Canvas(frame2, width=400, height=1280)
    w1.pack(anchor=W, expand=YES, fill=BOTH, side=LEFT)
    w1.create_line(0, 0, 200, 100)
    w1.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
    w1.bind("<Button-1>", click_on_image)

    global r_var
    r_var = IntVar()
    r_var.set(1)

    global info_col
    info_col = StringVar()
    info_col.set("Please load files first")

    Button(frame3, text="APPLY&Save", bg="green", command=button_apply2).pack(anchor=W, side=LEFT)
    Button(frame3, text="REM_last_pt", bg="red", command=lambda: button_rem_mark(1)).pack(anchor=W, side=LEFT)
    Button(frame3, text="REM_all_pts", bg="red", command=lambda: button_rem_mark(0)).pack(anchor=W, side=LEFT)
    Button(frame3, text="Save_marks", bg="green", command=lambda: button_saverestoremark(1)).pack(anchor=W, side=LEFT)
    Button(frame3, text="Restore_marks", bg="green", command=lambda: button_saverestoremark(0)).pack(anchor=W,
                                                                                                     side=LEFT)

    Label(frame3, textvariable=info_col).pack(anchor=W, side=LEFT)
    Button(frame3, text="open PCD file", command=load_filePCD, width=10).pack(anchor=N, side=RIGHT)
    Button(frame3, text="open IMG_file", command=lambda: load_fileJPG(1), width=10).pack(anchor=N, side=RIGHT)
    Button(frame3, text="fast.pcd+  png", command=lambda: load_filePCD(0), width=10).pack(anchor=N, side=RIGHT)
    Button(frame3, text="L_next_w_Aruco", command=lambda: load_filePCD(3), width=10).pack(anchor=N, side=RIGHT)


    Checkbutton(frame1, text="S_Rp", variable=check_reproj_var, command=lambda: redraw_image()).pack(
        anchor=W, side=LEFT)

    button_clear(0)
    master.mainloop()


if __name__ == "__main__":
    main()

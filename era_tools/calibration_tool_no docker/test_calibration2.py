#!/usr/bin/env python3
# -*- coding: UTF-8 -*-


import configparser
import json

import numpy as np
import open3d as lib3d

#

def make_transform4(alpha, beta, gamma, Dx=0., Dy=0., Dz=0.):
    flip_transform_x = [[1, 0, 0, 0],
                        [0, np.cos(alpha), -np.sin(alpha), 0],
                        [0, np.sin(alpha), np.cos(alpha), 0],
                        [0, 0, 0, 1]]

    flip_transform_y = [[np.cos(beta), 0, np.sin(beta), 0],
                        [0, 1, 0, 0],
                        [-np.sin(beta), 0, np.cos(beta), 0],
                        [0, 0, 0, 1]]

    flip_transform_z = [[np.cos(gamma), -np.sin(gamma), 0, 0],
                        [np.sin(gamma), np.cos(gamma), 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1]]

    flip_tot = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    flip_tot = np.dot(flip_tot, flip_transform_y)
    flip_tot = np.dot(flip_tot, flip_transform_x)
    flip_tot = np.dot(flip_tot, flip_transform_z)

    return flip_tot


def make_transform(alpha, beta, gamma, Dx=0., Dy=0., Dz=0.):
    flip_transform_x = [[1, 0, 0],
                        [0, np.cos(alpha), -np.sin(alpha)],
                        [0, np.sin(alpha), np.cos(alpha)]]

    flip_transform_y = [[np.cos(beta), 0, np.sin(beta)],
                        [0, 1, 0],
                        [-np.sin(beta), 0, np.cos(beta)]]

    flip_transform_z = [[np.cos(gamma), -np.sin(gamma), 0],
                        [np.sin(gamma), np.cos(gamma), 0],
                        [0, 0, 1]]

    flip_tot = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]

    flip_tot = np.dot(flip_tot, flip_transform_y)
    flip_tot = np.dot(flip_tot, flip_transform_x)
    flip_tot = np.dot(flip_tot, flip_transform_z)

    return flip_tot


def main():
    # result of calibration from, rootcalibrator_r.py
    inputname_config = './result/lid2robot2desc.txt'

    config = configparser.ConfigParser()
    config.read(inputname_config)


    # ___________________________________________________________________________
    # PLEASE PLACE LIDAR FILENAME HERE
    inputname_pcd = './data/fly2/_6.pcd'
    # ___________________________________________________________________________
    # ___________________________________________________________________________


#   Control points coordinates
    point6 = [-611, -2028, 869]
    point5 = [-1082, -2002, 546]
    point4 = [-1243.37, -1842.12, 545.85]
    point3 = [-1345.31, -1974.47, 547.55]
    point1 = [-1345.31, -1974.47, 547.55]

    #   please replace to actual
    online_point = point6





# информация об угле поворота лидара относительно робота
# вычисляется как разница угла поворота калибровочной доски отсносительно лидара в процессе ее поиска (autobox)
# и угла поворота инструмента робота ( параметр "a")

# information about the angle of rotation of the lidar respecting to the robot
# it is calculated as the difference of the angle of rotation of the board relative to the lidar during autobox()
# and the rotation angle of the robot tool (parameter "a")



    robot2descrot  = json.loads(config['DEFAULT']['rotation_l2r'])
    tilt = json.loads(config['DEFAULT']['tilt_l2d'])
    lidar2desctrans = json.loads(config['DEFAULT']['translation_l2r'])

    print(robot2descrot)



#    Матрица преобразования координат робот - изображение- ( метры в миллиметры)
    # [right, up, back]
    change_axisr2l = np.array([[-0.001, 0, 0],
                               [0, 0, 0.001],
                               [0, 0.001, 0]])

#    Матрица преобразования изображение робот - изображение- ( метры в миллиметры вращаем координаты как надо куке)
#   Matrix of coord transformation kuka - image

    change_axisl2r = np.array([[-1000, 0, 0],
                               [0, 0, 1000],
                               [0, 1000, 0]])



    lidar = lib3d.PointCloud()
    lidar = lib3d.read_point_cloud(inputname_pcd)  # point cloud to process
    lidar.transform(make_transform4(-np.pi / 2, 0, 0))
#        x = left/right
#        y = up/down
#        -z = forward/back


    transform3 = make_transform(0, robot2descrot, -tilt)


    (x0,y0,z0) = lidar2desctrans


    conner0mesh = lib3d.create_mesh_coordinate_frame(size=2, origin =  lidar2desctrans)
    conner1mesh = lib3d.create_mesh_coordinate_frame(size=2, origin = [x0,y0,z0])
    lidar_mesh = lib3d.create_mesh_coordinate_frame(size=2, origin = [0,0,0])

    lib3d.draw_geometries([lidar,conner0mesh,conner1mesh,lidar_mesh])

    lidar2 = []
    for (x, y, z) in lidar.points:
        line = np.array([x - x0, y - y0, z - z0])  # go to robot lidar coord
        (x, y, z,) = np.dot(np.asarray(line),
                              transform3)
        lidar2.append([x, y, z])
    lidar_res = lib3d.PointCloud()
    lidar_res.points = lib3d.Vector3dVector(lidar2)
#    lib3d.draw_geometries([lidar_res,  lidar_mesh])


#   Rotation from image coord system to robot coors system,

    lidar3 = []
    for (x, y, z) in lidar_res.points:
        line = np.array([x, y, z])  # transform image coords 2 robot coords
        (x, y, z) = np.dot(np.asarray(line), change_axisl2r)
        lidar3.append([x, y, z])

    robot_res = lib3d.PointCloud()
    robot_res.points = lib3d.Vector3dVector(lidar3)

    board_mesh = lib3d.create_mesh_coordinate_frame(size=2000, origin = online_point)
    lib3d.draw_geometries([robot_res,board_mesh])


    exit(0)


def pick_points(pcd):
    print("")
    print("1) Please click [shift + left click]")
    vis = lib3d.VisualizerWithEditing()
    vis.create_window()

    vis.add_geometry(pcd)
    vis.run()  # user picks points
    vis.destroy_window()
    print("")
    return vis.get_picked_points()


if __name__ == "__main__":
    main()

# Calibration Tool

The finalized Calibration tool between Lidar, Stereo Camera and Robot

### Project structure description ###

docker-compose.yml - services named `pcl`, `root`, `test`

## Catalogues ##
* **data/input** - defined input data directory in the ini file
* **data/output** - defined output data directory in the ini file
* **src** - source python scripts
* **docker-commands** - build and push commands

## Run scripts ##
* **run_pcl.sh** - runs `one_XYZ_PCL.py` source script
* **run_root.sh** - runs `rootcalibrator.py` source script
* **run_test.sh** - runs `test_calibration.py` source script

#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import os
import configparser
import json
import pickle
from tkinter import *
from tkinter import messagebox
from tkinter.filedialog import askopenfilename

import cv2
import numpy as np
import open3d as lib3d
from PIL import ImageTk, Image
from scipy import stats
import time

# global variables definition

input_path = None
output_path = None

rot_step = np.pi / 48.

rotangle = [0, 0, 0, 8]
rotcenter = [0., 0., 0.]
rotAnchor = [0., 0., 0.]

x_width = 1280
y_height = 720
descsize = [0.56, 0.56]  # size of boad in Meters horizontal,vetrical

rotated_cl = []
unrotated_cl = []
scanning = []
marks = []
flag_markstate = 0
max_zone_radius = 4
CameraMatrix2 = []
CameraDistorsion = []

info_col = None
global_mode = None

w = None
w1 = None
tkimage = None
img = None
pcd_h_read = None
autoclickpt = None

rot_corr = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]], np.float64)  # rotation vector zero rotation
trans_corr = np.array([0, 0, 0], np.float64)  # translation vector zero translation
flag_corr_ok = False
check_reproj_var = None

# for automatic descboard searching
lm = 0
rm = 0
uc = 0
dc = 0
rot = 0
tilt = 0
dcx = 0
dcy = 0
dcz = 0

# defalut filenames
defaultpcdfile = None
defaultimgfile = None


def autobox(mode=None, idxclick=0):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    global global_mode
    global scanning
    global pcd_h_read
    global marks
    global autoclickpt
    global descsize
    global rot
    global tilt
    global dcx
    global dcy
    global dcz

    if mode == 1:
        infopanel("please click to box lidar location", 1)
        global_mode = 'wait_click'

    cloud = np.delete(np.asarray(scanning, np.float64), 2, 1)
    descboard = []

    xx = np.float64(0.)
    yy = np.float64(0.)
    zz = np.float64(0.)

    if mode == 2:  # return after click

        pcd = lib3d.PointCloud()
        pcd.points = lib3d.Vector3dVector(cloud)
        pcd_tree = lib3d.KDTreeFlann(pcd)
        [k, idx, _] = pcd_tree.search_radius_vector_3d(pcd.points[idxclick], descsize[0])

        #[k, idx, _] = pcd_tree.search_knn_vector_3d(pcd.points[idxclick], 400)
        k = 0
        regrx = []
        regry = []
        regrz = []

        for id in idx:
            if np.abs( scanning[id][2]- scanning[idxclick][2]) < descsize[0]:
                scanning[id][3] = 255
                descboard.append([scanning[id][0], scanning[id][1], scanning[id][2]])
                xx += scanning[id][0]
                yy += scanning[id][1]
                zz += scanning[id][2]

                regrx.append(scanning[id][0])
                regry.append(scanning[id][1])
                regrz.append(scanning[id][2])
                k += 1

        xx = xx / k
        yy = yy / k
        zz = zz / k
        # marks.append([0, 0, [xx,yy,zz,0], None, None])

        # rotation angle estimation
        rot = 0.
        nrot = 0.




        gradient, intercept, r_value, p_value, std_err = stats.linregress(regrx, regrz)
        gradient2, intercept2, r_value2, p_value2, std_err2 = stats.linregress(regry, regrz)

        rot = np.arctan(gradient)
        tilt = -np.arctan(gradient2)*np.sin(rot)

        redraw_pcd(tilt,-rot,angle_z,scale)
        time.sleep(5)

        print("ROTATION: ", -rot)
        print("TILT: ", np.arctan(gradient2))
        print("ROTATION AXIS FROM LIDAR 2 DEPT_CAMERA MATRIX:\n",make_transform(-np.pi/2,0,0))
        print("2 ROBOT COORD ROTATION MATRIX:\n", make_transform(tilt,-rot,0 ))

        print("TRANSLATION VECTOR: ", (xx,yy,zz) )


        file = open(os.path.join(output_path, "./lidarpos.txt"), "wt")
        file.write("ROTATION:" + str(np.arctan(gradient)))
        file.write("\nTILT:" + str(np.arctan(gradient2)))
        file.close()


        dcx = xx  # left-right
        dcy = yy  # foeward-back - z ui world
        dcz = zz  # up down - y in world coords

        marks.append(
            [-1, 0, [xx , yy - descsize[0], zz+descsize[0]*np.sin(tilt), 0], None, None])  # DC
        marks.append(
            [-2, 0, [xx , yy + descsize[0], zz-descsize[0]*np.sin(tilt), 0], None, None])  # UC
        #  marks.append([-2, 0, [xx , yy + descsize[0] * np.sin(tilt), zz + descsize[0] , 0], None, None])  # UC

        marks.append([-3, 0, [xx - descsize[1] * np.cos(rot), yy, zz - descsize[1] * np.sin(rot), 0], None, None])
        marks.append([-4, 0, [xx + descsize[1] * np.cos(rot), yy, zz + descsize[1] * np.sin(rot), 0], None, None])

        redraw_pcd(angle_x, angle_y, angle_z, scale)
        infopanel("please follow to image corresponding green markers", 1)
        global_mode = 'wait_click_4img'
        info_col.set("Please select 4 point on camera panel")
        autoclickpt = -4
        #redraw_pcd(-tilt,0,-rot,10)
        #time.sleep(5)

    return


# main calibration function
def button_apply2():
    points_3d = []
    points_2d = []

    global rot_corr
    global trans_corr
    global marks
    global flag_corr_ok
    global rotangle

    gamma = np.pi / 4  # rotation will be calculated from -x radar axis
    rotation_z = make_transform(0, 0, gamma)

    for lines in marks:
        (xz, yz, zz, intensiv) = lines[2]
        line = np.array([xz, yz, zz, intensiv])
        rotated = np.dot(line, rotation_z)

        (x, y, z, i_light) = rotated
        xp, yp = (lines[3], lines[4])
        mtx = np.asarray(CameraMatrix2, np.float64)
        dist = np.asarray(CameraDistorsion, np.float64)
        # append points
        points_3d.append([-x, z, y])  # pattern points
        points_2d.append([xp, yp])  # camera points

    objpoints = np.asarray(points_3d)  # (xyz) points of object
    objpoints = objpoints.astype('float64')
    imgpoints = np.asarray(points_2d)  # lidar = left camera
    imgpoints = imgpoints.astype('float64')

    mtx = np.asarray(CameraMatrix2)  # CameraMatrix
    dist = np.asarray(CameraDistorsion)  # # distorsion matrix

    # _ret, rot_corr, trans_corr, inliers = cv2.solvePnPRansac(objpoints, imgpoints, mtx, dist)

    _ret, rot_corr, trans_corr = cv2.solvePnP(objpoints, imgpoints, cameraMatrix=mtx, distCoeffs=dist,
                                              flags=cv2.SOLVEPNP_ITERATIVE)
    print(_ret)
    _ret = True
    if _ret == True:
        flag_corr_ok = True
        print("ROTATION_CORRECTED:")
        print(rot_corr)
        file = open(os.path.join(output_path, "./rotation.txt"), "wt")
        file.write(str(rot_corr))
        file.close()

        print("TRANSLATION_CORRECTED:")
        print(trans_corr)
        file = open(os.path.join(output_path, "./translation.txt"), "wt")
        file.write(str(trans_corr))
        file.close()

    redraw_image()
    return


def infopanel(message, type="error"):
    if type == 'error':
        messagebox.showerror("Error", message)

    else:
        messagebox.showinfo("Information", message)

    return


def button_rem_mark(mode=0):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    global marks
    global flag_markstate

    if mode == 0:
        marks.clear()
        flag_markstate = 0
    if mode == 1:
        if len(marks) > 0:
            marks.pop()
        flag_markstate = 0

    redraw_pcd(angle_x, angle_y, angle_z, scale)
    redraw_image()

    return


def button_saverestoremark(mode=0):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    global marks
    global flag_markstate

    if mode == 0:  # load_marks
        marks.clear()
        try:
            with open(os.path.join(input_path, 'marks.dat'), 'rb') as fmarks:
                marks = pickle.load(fmarks)
        except FileNotFoundError:
            infopanel("Error filereading marks", "error")

            # print(marks)

    if mode == 1:  # save_marks

        if flag_markstate != 0:
            infopanel("Can't save marks, please finish pair click", "error")
            return

        fmarks = open(os.path.join(input_path, 'marks.dat'), 'wb')
        pickle.dump(marks, fmarks, protocol=pickle.HIGHEST_PROTOCOL)
        fmarks.close()

    redraw_pcd(angle_x, angle_y, angle_z, scale)
    redraw_image()

    return


def click_on_image(event):
    global info_col
    global flag_markstate
    global marks
    global global_mode
    global autoclickpt
    global rotangle

    global lm
    global rm
    global uc
    global dc
    global descsize
    global rot
    global tilt
    global dcx
    global dcy
    global dcz

    (angle_x, angle_y, angle_z, scale) = rotangle

    if global_mode == 'wait_click_4img':
        for idx, pointline in enumerate(marks):
            if marks[idx][0] == autoclickpt:

                if autoclickpt == -4:
                    lm = idx
                elif autoclickpt == -3:
                    rm = idx
                elif autoclickpt == -2:
                    uc = idx
                elif autoclickpt == -1:
                    dc = idx
                marks[idx][3] = event.x
                marks[idx][4] = event.y
                marks[idx][0] = 0
                autoclickpt += 1

                redraw_pcd(angle_x, angle_y, angle_z, scale)

                #                      -2
                #                    xuc yuc
                #        -4 xlm,ylm            xrm,yrm -3
                #                     xdc ydc
                #                       -1
                #

                if autoclickpt == 0:
                    autoclickpt = None
                    global_mode = None

                    xlm = marks[lm][3]
                    ylm = marks[lm][4]

                    xrm = marks[rm][3]
                    yrm = marks[rm][4]

                    xuc = marks[uc][3]
                    yuc = marks[uc][4]

                    xdc = marks[dc][3]
                    ydc = marks[dc][4]

                    marks.append([0, 0, [dcx, dcy, dcz, 0], (xuc + xdc) / 2., (ylm + yrm) / 2.])

#                    for ppt in range(1, 5):
#                        marks.append(
#                            [0, 0,
#                             [dcx - descsize[1] * np.sin(rot) / 5. * ppt, dcy + descsize[0] * np.cos(rot) / 5. * ppt,
#                              dcz, 0],
#                             (xuc + xdc) / 2. + (xrm - xlm) / 10. * ppt, (ylm + yrm) / 2. + (yrm - ylm) / 10. * ppt])
#                        marks.append(
#                            [0, 0,
#                             [dcx + descsize[1] * np.sin(rot) / 5. * ppt, dcy - descsize[0] * np.cos(rot) / 5. * ppt,
#                              dcz,
#                              0], (xuc + xdc) / 2. - (xrm - xlm) / 10. * ppt,
#                             (ylm + yrm) / 2. - (yrm - ylm) / 10. * ppt])

#                        marks.append([0, 0,
#                                      [dcx ,
#                                       dcy,
#                                       dcz - descsize[0] / 5. * ppt, 0],
#                                      (xuc + xdc) / 2. - (xuc - xdc) / 10. * ppt,
#                                      (ylm + yrm) / 2. + (ydc - yuc) / 10. * ppt])
#
#                        marks.append([0, 0,
#                                      [dcx,
#                                       dcy,
#                                       dcz + descsize[0] / 5. * ppt, 0],
#                                      (xuc + xdc) / 2. + (xuc - xdc) / 10. * ppt,
#                                      (ylm + yrm) / 2. - (ydc - yuc) / 10. * ppt])

                    #                        marks.append([0, 0, [dcx, dcy , dcz - descsize[1] / 5. * ppt, 0],
                    #                                      (xuc + xdc) / 2. - (xuc - xdc) / 10. * ppt,
                    #                                      (ylm + yrm) / 2. + (ydc - yuc) / 10. * ppt])
                    #                        marks.append(
                    #                            [0, 0, [dcx, dcy , dcz + descsize[1] / 5. * ppt, 0],
                    #                             (xuc + xdc) / 2. + (xuc - xdc) / 10. * ppt,
                    #                             (ylm + yrm) / 2. - (ydc - yuc) / 10. * ppt])

                    # marks.append(
                    #                            [0, 0,
                    #                             [dcx + descsize[1] * np.sin(rot) / 5. * ppt, dcy - descsize[0] * np.cos(rot) / 5. * ppt,
                    #                              dcz, 0],
                    #                             (ylm + yrm) / 2. + , (ylm+yrm)/2. - ])

                    redraw_pcd(angle_x, angle_y, angle_z, scale)

                break

    else:
        if flag_markstate == 0:
            info_col.set("Please select point on lidar panel first")
            return
        info_col.set("Please select pair point on image panel")
        marks[-1][3] = event.x
        marks[-1][4] = event.y
        flag_markstate = 0
        info_col.set("Please select next point on lidar panel")
    redraw_image()


def make_transform(alpha, beta, gamma, Dx = 0., Dy = 0., Dz = 0.):
    flip_transform_x = [[1, 0, 0, 0],
                        [0, np.cos(alpha), -np.sin(alpha), 0],
                        [0, np.sin(alpha), np.cos(alpha), 0],
                        [0, 0, 0, 1]]

    flip_transform_y = [[np.cos(beta), 0, np.sin(beta), 0],
                        [0, 1, 0, 0],
                        [-np.sin(beta), 0, np.cos(beta), 0],
                        [0, 0, 0, 1]]

    flip_transform_z = [[np.cos(gamma), -np.sin(gamma), 0, 0],
                        [np.sin(gamma), np.cos(gamma), 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1]]


    flip_tot = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]


    flip_tot = np.dot(flip_tot, flip_transform_y)
    flip_tot = np.dot(flip_tot, flip_transform_x)
    flip_tot = np.dot(flip_tot, flip_transform_z)
#    flip_tot = np.dot(flip_tot, flip_trans)
#    print(alpha,beta,gamma)
    return flip_tot


def redraw_pcd(alpha, beta, gamma, scale):
    global pcd_h_read
    global tkimage
    global rotated_cl
    global unrotated_cl
    global info_col
    global scanning
    global autoclickpt
    global rotcenter

    colors = [(255, 255, 255), (255, 0, 255), (0, 255, 255), (255, 255, 0), (255, 0, 0), (127, 0, 0)]

    blank_image = np.zeros((y_height, x_width, 3), np.uint8)

    rotated_cl = []
    unrotated_cl = []
    (Dx, Dy, Dz) = rotcenter

    rotation_tot = make_transform(alpha, beta, gamma, Dx, Dy, Dz)

    for idx, trio in enumerate(scanning):
        line = np.array([trio[0], trio[1], trio[2], trio[3]])
        rotated = np.dot(line, rotation_tot)


        (x, y, z, i_light) = rotated

        # if z <= 0:
        #    continue  # I haven't eye on the nape

        yz = int(400 / 2 - 800 * y / scale / 2)
        xz = int(800 * x / scale / 2 + 800 / 2)

        if xz <= 0:
            continue
        if yz <= 0:
            continue

        if xz >= x_width:
            continue
        if yz >= y_height:
            continue

        # blank_image[yz][xz] = colors[int(i_light)]
        if i_light!= 255:
            if z<8:
                blank_image[yz][xz] = (255*(8-np.abs(z))/7,255*(8-np.abs(z))/7,255)
        else:
            blank_image[yz][xz] = (255,0,0)



        # закинули в словарики для быстрого анализа клика
        rotated_cl.append((xz, yz, z))
        unrotated_cl.append(idx)

    pclimg = Image.fromarray(blank_image)
    tkimage = ImageTk.PhotoImage(image=pclimg)
    w.delete(all)
    w.create_image(0, 0, image=tkimage, anchor="nw")
    w.update()

    # draw points found

    for lines in marks:
        (xz, yz, zz, intensiv) = lines[2]
        line = np.array([xz, yz, zz, intensiv])
        rotated = np.dot(line, rotation_tot)
        (x, y, z, i_light) = rotated

        #        if z <= 0:
        #            continue  # I haven't eye on the nape


        yz = int(400 / 2 - 800 * y / scale / 2)
        xz = int(800 * x / scale / 2 + 800 / 2)

        if xz <= 0:
            continue
        if yz <= 0:
            continue

        if xz >= x_width:
            continue
        if yz >= y_height:
            continue
        if lines[0] == autoclickpt:
            w.create_oval(xz - 5, yz - 5, xz + 5, yz + 5, fill="#00FF00")
        else:
            w.create_oval(xz - 5, yz - 5, xz + 5, yz + 5, fill="#FF6042")
    # draw status
    if global_mode == 'wait_click_4img':
        info_col.set("Please select pair point on camera panel (marked green)")
    else:
        if flag_markstate == 0:
            info_col.set("Please select point on lidar panel")
        if flag_markstate == 1:
            info_col.set("Please select point on camera panel")

    # print("Rot:{} {} {}".format(alpha,beta,gamma))
    return


def trans_move(a, b, c, d):
    global trans_corr
    global marks
    global rotangle
    global global_mode
    global tilt

    if global_mode != 'wait_click_4img':
        infopanel("Actual only when Autobox mode", 0)
        return

    (angle_x, angle_y, angle_z, scale) = rotangle

    for idx, pointline in enumerate(marks):
        if marks[idx][0] < 0:
            #print(marks[idx][2])
            marks[idx][2][0] += 0.05 * a
            marks[idx][2][1] += 0.05 * b
            marks[idx][2][2] += 0.05 * c
    redraw_pcd(angle_x, angle_y, angle_z, scale)


def redraw_image():
    global w1
    global marks
    global CameraMatrix2
    global rotangle

    global x_width
    global y_height

    global rot_corr
    global trans_corr
    global tvec
    global flag_corr_ok
    global scanning
    global rotangle
    global check_reproj_var

    # (alpha,beta,gamma,scale) = rotangle
    gamma = np.pi / 4.
    rotation_z = make_transform(0, 0, gamma)
    # rotation_x =  make_transform(alpha, 0, 0)
    # rotation_y = make_transform(0,beta, 0)

    w1.delete("all")
    w1.create_image(0, 0, image=img, anchor="nw")
    w1.update()

    mtx = np.asarray(CameraMatrix2, np.float64)
    dist = np.asarray(CameraDistorsion, np.float64)

    for lines in marks:
        (xz, yz, zz, intensiv) = lines[2]
        line = np.array([xz, yz, zz, intensiv])
        rotated = np.dot(line, rotation_z)
        (x, y, z, i_light) = rotated
        xp, yp = (lines[3], lines[4])
        if xp == None or yp == None:
            continue
        w1.create_oval(xp - 5, yp - 5, xp + 5, yp + 5, fill="#FF6042")

        coordstr = np.asarray([[-x, z, y]], np.float64)

        result1 = cv2.projectPoints(coordstr,
                                    rot_corr,
                                    trans_corr,
                                    mtx,
                                    dist)

        (xx, yy) = result1[0][0][0]
        w1.create_oval(xx - 5, yy - 5, xx + 5, yy + 5, fill="#00FF00")

    if flag_corr_ok == True:
        for idx, trio in enumerate(scanning):
            line = np.array([trio[0], trio[1], trio[2], trio[3]])
            rotated = np.dot(line, rotation_z)
            (x, y, z, i_light) = rotated
            coordstr = np.asarray([[-x, z, y]], np.float64)

            result1 = cv2.projectPoints(coordstr,
                                        rot_corr,
                                        trans_corr,
                                        mtx,
                                        dist)
            (xx, yy) = result1[0][0][0]

            if 0 < xx < x_width and 0 < yy < y_height and y < 0 and check_reproj_var.get() == 1:
                w1.create_oval(xx - 1, yy - 1, xx, yy, width=1, outline='white')

        xp = -0.5
        yp = -0.5


def load_fileJPG(mode=1):
    if mode == 1:
        fname = askopenfilename(initialdir=input_path, filetypes=(("PNG files", "*.png"), ("JPG files", "*.jpg"),
                                           ("All files", "*.*")))
    else:
        fname = defaultimgfile
    #print(fname)
    global img
    global w1

    sourceimg = Image.open(fname)
    [imageSizeWidth, imageSizeHeight] = sourceimg.size
    tag = sourceimg.resize((int(imageSizeWidth), int(imageSizeHeight)), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(tag)
    w1.create_image(0, 0, image=img, anchor="nw")

    w1.update()


def button_rot_left():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_y > -np.pi:
        rotangle[1] -= rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_up():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_x < np.pi:
        rotangle[0] += rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_down():
    """rot down"""
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_x > -np.pi:
        rotangle[0] -= rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_right():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_y < np.pi:
        rotangle[1] += rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_scale_inc():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle
    if scale < 1:
        scale *= 2

    elif scale < 20:
        rotangle[3] += 1
    redraw_pcd(angle_x, angle_y, angle_z, scale)
    return


def button_rot_z(direction):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if abs(angle_z) <= np.pi:
        rotangle[2] += int(direction) * rot_step / 2.
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_scale_dec():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if scale > 1:
        rotangle[3] -= 1
    else:
        rotangle[3] /= 2.

    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])


def button_clear(mode):
    global rotangle

    if mode == 0:
        rotangle = [-0.5235987755982988, 0, 0, 6]
        redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])

    if mode == 1:
        rotangle = [np.pi / 2., 0, 0, 9]
        redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])



def gen_test_pcl():
    global pcd_h_read
    global rotangle
    global scanning

    (angle_x, angle_y, angle_z, scale) = rotangle
    scanning.clear()

    for x in range(-20, 20, 4):
        for y in range(-20, 20, 4):
            for z in range(0, 2, 1):
                if x == 0:
                    scanning.append([x / 5., y / 5., z / 8., 1])
                elif y == 0:
                    scanning.append([x / 5., y / 5., z / 8., 2])
                else:
                    scanning.append([x / 5., y / 5., z / 8., 0])
    redraw_pcd(angle_x, angle_y, angle_z, scale)


def load_filePCD(mode=1):
    global pcd_h_read

    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle
    global scanning
    global max_zone_radius
    global defaultimgfile
    global defaultpcdfile

    scanning.clear()

    beta = 0

    flip_transform = make_transform(-np.pi/2,0,0)

    if mode == 1:
        fname = askopenfilename(initialdir=input_path, filetypes=(("PCD", "*.pcd"), ("All files", "*.*")))
    else:
        fname = defaultpcdfile
        load_fileJPG(0)
        button_saverestoremark(0)

    pcd_h_read = lib3d.read_point_cloud(fname)

    pcd_h_read.transform(flip_transform)


    num_readed = np.asarray(pcd_h_read.points)
    for (xl, yl, zl) in num_readed:
        scanning.append([xl, yl, zl, 1])


    print(str(len(scanning)) + " points read")
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def click_analyze(event):
    """
    trying to find something near mouse click

    :param event:
    """
    # координаты клика
    global rotated_cl
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle
    global marks
    global flag_markstate
    global info_col
    global global_mode

    if flag_markstate == 1 and global_mode == None:
        info_col.set("Please select pair point on image panel, lidar panel loked!")
        return

    delta = 2  # area of search delta x delta points

    x, y = (event.x), (event.y)

    # trying to find something testy
    for idx, (sx, sy, sz) in enumerate(rotated_cl):

        if abs(x - sx) < delta and abs(y - sy) < delta:
            position = unrotated_cl[idx]
            if global_mode == None:
                scanning[position][3] = 2  # mark points
                marks.append([x, y, scanning[position], None, None])

                flag_markstate = 1
                info_col.set("Please select pair point on image panel")
            if global_mode == 'wait_click':
                autobox(2, position)
            break

    redraw_pcd(angle_x, angle_y, angle_z, scale)
    return


def ReadConfigfile():
    global CameraMatrix2
    global CameraDistorsion
    global descsize
    global defaultpcdfile
    global defaultimgfile
    global input_path
    global output_path

    config = configparser.ConfigParser()
    config.read('config.ini')

    CameraMatrix2 = json.loads(config['DEFAULT']['CameraMatrix'])
    descsize = json.loads(config['DEFAULT']['CalibrationDescSize'])
    defaultimgfile = config['DEFAULT']['DefaultImgFile']
    defaultpcdfile = config['DEFAULT']['DefaultPcdFile']

    input_path = config['DEFAULT']['InputPath']
    output_path = config['DEFAULT']['OutputPath']

    #print(CameraDistorsion)

    #print(CameraMatrix2)
    return


def main():
    cv_version = cv2.__version__
    family = cv_version.split('.')
    if int(family[0]) < 3:
        print("Please install opencv 3.0 and newer. Can't continue")
        return
    ReadConfigfile()
    master = Tk()
    master.resizable(1280, 720)

    global w
    global w1
    global check_reproj_var

    check_reproj_var = IntVar()
    check_reproj_var.set(1)

    frame1 = Frame(master, relief=RAISED, borderwidth=1)
    frame1.pack(fill=BOTH, expand=True, side=TOP)
    frame3 = Frame(master, relief=RAISED, borderwidth=1)
    frame3.pack(fill=BOTH, expand=True, side=TOP)

    frame2 = Frame(master, relief=RAISED, borderwidth=1)
    frame2.pack(fill=BOTH, expand=True, side=TOP)

    Label(frame1, text="Lidar viw control:").pack(anchor=W, side=LEFT)
    Button(frame1, text="<-", bg="blue", fg='white', command=button_rot_left, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="^", bg="blue", fg='white', command=button_rot_up, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="v", bg="blue", fg='white', command=button_rot_down, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="->", bg="blue", fg='white', command=button_rot_right, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="Rot R", bg="blue", fg='white', command=lambda: button_rot_z(1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="Rot L", bg="blue", fg='white', command=lambda: button_rot_z(-1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)

    Button(frame1, text="-", bg="green", fg='white', command=button_scale_inc, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="+", bg="green", fg='white', command=button_scale_dec, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="Top View", bg="red", fg='white', command=lambda: button_clear(1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="DEPT_V", bg="red", fg='white', command=lambda: button_clear(0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="AutoBox", bg="yellow", fg='black', command=lambda: autobox(1)).pack(anchor=W, side=LEFT)

    w = Canvas(frame2, width=400, height=300)
    w.pack(expand=YES, fill=BOTH, side=TOP)
    w.create_line(0, 0, 200, 100)
    w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
    # w.create_rectangle(50, 25, 150, 75, fill="blue")
    w.bind("<Button-1>", click_analyze)

    w1 = Canvas(frame2, width=400, height=1280)
    w1.pack(anchor=W, expand=YES, fill=BOTH, side=LEFT)
    w1.create_line(0, 0, 200, 100)
    w1.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
    w1.bind("<Button-1>", click_on_image)

    global r_var
    r_var = IntVar()
    r_var.set(1)

    global info_col
    info_col = StringVar()
    info_col.set("Please load files first")

    Button(frame3, text="APPLY&Save", bg="green", command=button_apply2).pack(anchor=W, side=LEFT)
    Button(frame3, text="REM_last_pt", bg="red", command=lambda: button_rem_mark(1)).pack(anchor=W, side=LEFT)
    Button(frame3, text="REM_all_pts", bg="red", command=lambda: button_rem_mark(0)).pack(anchor=W, side=LEFT)
    Button(frame3, text="Save_marks", bg="green", command=lambda: button_saverestoremark(1)).pack(anchor=W, side=LEFT)
    Button(frame3, text="Restore_marks", bg="green", command=lambda: button_saverestoremark(0)).pack(anchor=W,
                                                                                                     side=LEFT)

    Label(frame3, textvariable=info_col).pack(anchor=W, side=LEFT)
    Button(frame3, text="open PCD file", command=load_filePCD, width=10).pack(anchor=N, side=RIGHT)
    Button(frame3, text="open IMG_file", command=lambda: load_fileJPG(1), width=10).pack(anchor=N, side=RIGHT)
    Button(frame3, text="fast.pcd+  png", command=lambda: load_filePCD(0), width=10).pack(anchor=N, side=RIGHT)
    Label(frame1, text="Abox_M_cor:").pack(anchor=W, side=LEFT)
    Button(frame1, text="x-", bg="gray", fg='white', command=lambda: trans_move(-1, 0, 0, 0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="x+", bg="gray", fg='white', command=lambda: trans_move(1, 0, 0, 0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="y-", bg="gray", fg='white', command=lambda: trans_move(0, -1, 0, 0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="y+", bg="gray", fg='white', command=lambda: trans_move(0, 1, 0, 0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="z-", bg="gray", fg='white', command=lambda: trans_move(0, 0, -1, 0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="z+", bg="gray", fg='white', command=lambda: trans_move(0, 0, 1, 0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)

    Checkbutton(frame1, text="S_Rp", variable=check_reproj_var, command=lambda: redraw_image()).pack(
        anchor=W, side=LEFT)

    button_clear(0)
    master.mainloop()


if __name__ == "__main__":
    main()
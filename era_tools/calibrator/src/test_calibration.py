import configparser
import json
import pickle
from tkinter import *
from tkinter import messagebox
from tkinter.filedialog import askopenfilename

import numpy as np
import open3d as lib3d
from PIL import ImageTk, Image
from scipy import stats
import time



def make_transform(alpha, beta, gamma, Dx = 0., Dy = 0., Dz = 0.):
    flip_transform_x = [[1, 0, 0, 0],
                        [0, np.cos(alpha), -np.sin(alpha), 0],
                        [0, np.sin(alpha), np.cos(alpha), 0],
                        [0, 0, 0, 1]]

    flip_transform_y = [[np.cos(beta), 0, np.sin(beta), 0],
                        [0, 1, 0, 0],
                        [-np.sin(beta), 0, np.cos(beta), 0],
                        [0, 0, 0, 1]]

    flip_transform_z = [[np.cos(gamma), -np.sin(gamma), 0, 0],
                        [np.sin(gamma), np.cos(gamma), 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1]]


    flip_tot = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]


    flip_tot = np.dot(flip_tot, flip_transform_y)
    flip_tot = np.dot(flip_tot, flip_transform_x)
    flip_tot = np.dot(flip_tot, flip_transform_z)
#    flip_tot = np.dot(flip_tot, flip_trans)
#    print(alpha,beta,gamma)
    return flip_tot


def main():

    lid_rot = -0.30299890285613534
    lid_tilt = 0.16187814413221452
    #lid_rot = 0
    #lid_tilt = 0


    lidar_center =  [2.762515495380435, -0.49930698525815215, -2.4444483444293454]

    lidar = lib3d.PointCloud()
    lidar_res = lib3d.PointCloud()
    lidar = lib3d.read_point_cloud("../data/input/lid/pcldata_1544199305_1.pcd")
    lidar.transform(make_transform(-np.pi/2,0,0))
    lidar2 = []
    transform = make_transform(lid_tilt, lid_rot, 0)
    for (x, y, z) in lidar.points:

        line = np.array([x - lidar_center[0], y - lidar_center[1], z - lidar_center[2],0])
        (x,y,z,i) = np.dot(line,transform)
        lidar2.append([x,y,z])

    lidar_res.points = lib3d.Vector3dVector(lidar2)
    lib3d.draw_geometries([lidar_res])

    zed_rot = -0.9198204097562191
    zed_tilt = 0.1704385108412625

    #zed_rot = 0
    #zed_tilt = 0

    zed_center = [0.004582881774784228, 0.036305397539269695, -2.1976318292188437]

    zed = lib3d.PointCloud()
    zed_res = lib3d.PointCloud()
    zed = lib3d.read_point_cloud("../data/input/zed/point_cloud_PCD_14696_720_07-12-2018-17-15-30.pcd")
    zed = lib3d.voxel_down_sample(zed, voxel_size=0.05)
    zed2 = []
    transform = make_transform(zed_tilt, zed_rot, 0)
    for x, y, z in zed.points:
        line = np.array([x - zed_center[0], y - zed_center[1], z - zed_center[2],0])
        (x,y,z,i) = np.dot(line,transform)
        zed2.append([x,y,z])

    zed_res.points = lib3d.Vector3dVector(zed2)

    #zed_res.paint_uniform_color([1, 0.706, 0])
    lidar_res.paint_uniform_color([0, 0.651, 0.929])
    #lib3d.draw_geometries([zed_res])
    lib3d.draw_geometries([lidar_res,zed_res])








if __name__ == "__main__":
    main()

#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import os
import configparser
import json
from tkinter import *
from tkinter import messagebox
from tkinter.filedialog import askopenfilename


import numpy as np
import open3d as lib3d
from PIL import ImageTk, Image


# global variables definition

input_path = None
output_path = None

rot_step = np.pi / 48.

rotangle = [0, 0, 0, 40]
rotcenter = [0., 0., 0.]
rotAnchor = [0., 0., 0.]

x_width = 1000
y_height = 1000
descsize = [0.6, 0.6]  # size of boad in Meters horizontal,vetrical

rotated_cl = []
unrotated_cl = []
scanning = []
marks = []
flag_markstate = 0
max_zone_radius = 3

info_col = None
global_mode = None

w = None
w1 = None
tkimage = None
img = None
pcd_h_read = None
autoclickpt = None

rot_corr = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]], np.float64)  # rotation vector zero rotation
trans_corr = np.array([0, 0, 0], np.float64)  # translation vector zero translation
flag_corr_ok = False
check_reproj_var = None
colormap = None

# defalut filenames
defaultpcdfile = None
defaultimgfile = None
voxelfactor = 0.05


def autobox(mode=None, idxclick=0):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    global global_mode
    global scanning
    global pcd_h_read
    global marks
    global autoclickpt
    global descsize
    global rotcenter

    global XAncRes
    global YAncRes
    global ZAncRes
    global RotRes
    global TiltRes

    global XAnc
    global YAnc
    global ZAnc
    global RotAnc
    global TiltAnc

    if mode == 1:
        infopanel("please click to box location", 1)
        global_mode = 'wait_click'

    cloud = np.delete(np.asarray(scanning, np.float64), 2, 1)
    descboard = []

    xx = np.float64(0.)
    yy = np.float64(0.)
    zz = np.float64(0.)

    if mode == 2:  # return after click

        pcd = lib3d.PointCloud()
        pcd.points = lib3d.Vector3dVector(cloud)
        pcd_tree = lib3d.KDTreeFlann(pcd)

        # small radius
        [k, idx, _] = pcd_tree.search_radius_vector_3d(pcd.points[idxclick], descsize[0] * 0.2)
        for id in idx:
            descboard.append([scanning[id][0], scanning[id][1], scanning[id][2]])
        cb = lib3d.PointCloud()
        cb.points = lib3d.Vector3dVector(descboard)
        lib3d.estimate_normals(cb, search_param=lib3d.KDTreeSearchParamHybrid(10, max_nn=50))
        xx = 0.
        yy = 0.
        zz = 0.
        k = 0.

        for trio in cb.normals:
            xx += trio[0]
            yy += trio[1]
            zz += trio[2]
            k += 1

        # normx normy normz - norm of vector at the center desc board
        normx = xx / k
        normy = yy / k
        normz = zz / k

        #print("norm v", normx, normy, normz)

        xx = 0.
        yy = 0.
        zz = 0.
        k = 0.
        cb.clear()
        descboard = []

        # big radius

        [k, idx, _] = pcd_tree.search_radius_vector_3d(pcd.points[idxclick], descsize[0]*1.2)
        for id in idx:
            descboard.append([scanning[id][0], scanning[id][1], scanning[id][2]])
        cb.points = lib3d.Vector3dVector(descboard)
        lib3d.estimate_normals(cb, search_param=lib3d.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))

        xx = 0
        yy = 0
        zz = 0
        k = 0

        regrz = []
        regrx = []
        regry = []
        descboard = []

        #print(normx, normy, normz)
        #print("===================")
        for idx,(x,y,z) in enumerate(cb.normals):
            if np.abs(normx - x) < 0.1 and np.abs(normy - y) < 0.1 and np.abs(normz - z) < 0.1:
                descboard.append(cb.points[idx])
                #regrx.append(cb.points[idx][0])
                xx += cb.points[idx][0]
                #regry.append(cb.points[idx][1])
                yy += cb.points[idx][1]
                #regrz.append(cb.points[idx][2])
                zz += cb.points[idx][2]
                k += 1
        cb.clear()
        cb.points = lib3d.Vector3dVector(descboard)
        lib3d.estimate_normals(cb, search_param=lib3d.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
#        print(np.asarray(cb.normals))

#        lib3d.draw_geometries([cb])


        xx = xx / k
        yy = yy / k
        zz = zz / k
        marks.append([0, 0, [xx,yy,zz,0], None, None])


#        gradient, intercept, r_value, p_value, std_err = stats.linregress(regrx, regrz)
#        gradient2, intercept2, r_value2, p_value2, std_err2 = stats.linregress(regry, regrz)

        rot1 = np.arctan(normx/normz) + float(RotAnc.get())
        rot2 = - np.arctan(normy/normz)*np.cos(rot1)+float(TiltAnc.get())
#        print ("xz", np.arctan(normx/normz)," yz",np.arctan(normx/normy),"xy", np.arctan(normz/normy))

        marks.append([0, 0, [xx+descsize[0]*normx, yy + descsize[0]*normy, zz+descsize[0]*normz , 0], None, None])

        marks.append([-1, 0, [xx, yy + descsize[0], zz, 0], None, None])
        marks.append([-2, 0, [xx, yy - descsize[0], zz, 0], None, None])

        marks.append([-3, 0, [xx - descsize[1] * np.cos(-rot1), yy, zz - descsize[1] * np.sin(-rot1), 0], None, None])
        marks.append([-4, 0, [xx + descsize[1] * np.cos(-rot1), yy, zz + descsize[1] * np.sin(-rot1), 0], None, None])


        XAncRes.set(xx + float(XAnc.get()))
        YAncRes.set(yy + float(YAnc.get()))
        ZAncRes.set(zz + float(ZAnc.get()))
        RotRes.set(rot1 + float(RotAnc.get()))
        TiltRes.set(rot2 + float(TiltAnc.get()) )

        print("ROTATION:",rot1)
        print("TILT:",rot2)
        print("CENTER:",[xx +  float(XAnc.get()), yy +  float(YAnc.get()), zz +  float(ZAnc.get())])

#        rotangle = [angle_x, angle_y, angle_z, scale]
        rotangle = [rot2, rot1, 0, scale]
        rotcenter = [xx +  float(XAnc.get()), yy +  float(YAnc.get()), zz +  float(ZAnc.get())]
        (angle_x, angle_y, angle_z, scale) = rotangle
        redraw_pcd(angle_x, angle_y, angle_z, scale)

    return


def infopanel(message, type="error"):
    if type == 'error':
        messagebox.showerror("Error", message)

    else:
        messagebox.showinfo("Information", message)

    return


def button_saverestoremark(mode=0):
    return
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    global marks
    global flag_markstate

    if mode == 0:  # load_marks
        marks.clear()
        try:
            with open(os.path.join(input_path, 'marks.dat'), 'rb') as fmarks:
                marks = pickle.load(fmarks)
        except FileNotFoundError:
            infopanel("Error filereading marks", "error")

            # print(marks)

    if mode == 1:  # save_marks

        if flag_markstate != 0:
            infopanel("Can't save marks, please finish pair click", "error")
            return

        fmarks = open(os.path.join(input_path, 'marks.dat'), 'wb')
        pickle.dump(marks, fmarks, protocol=pickle.HIGHEST_PROTOCOL)
        fmarks.close()

    redraw_pcd(angle_x, angle_y, angle_z, scale)

    return


def click_on_image(event):
    return


def make_transform(alpha, beta, gamma, Dx = 0., Dy = 0., Dz = 0.):
    flip_transform_x = [[1, 0, 0, 0],
                        [0, np.cos(alpha), -np.sin(alpha), 0],
                        [0, np.sin(alpha), np.cos(alpha), 0],
                        [0, 0, 0, 1]]

    flip_transform_y = [[np.cos(beta), 0, np.sin(beta), 0],
                        [0, 1, 0, 0],
                        [-np.sin(beta), 0, np.cos(beta), 0],
                        [0, 0, 0, 1]]

    flip_transform_z = [[np.cos(gamma), -np.sin(gamma), 0, 0],
                        [np.sin(gamma), np.cos(gamma), 0, 0],
                        [0, 0, 1, 0],
                        [0, 0, 0, 1]]


    flip_tot = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]


    flip_tot = np.dot(flip_tot, flip_transform_y)
    flip_tot = np.dot(flip_tot, flip_transform_x)
    flip_tot = np.dot(flip_tot, flip_transform_z)
#    flip_tot = np.dot(flip_tot, flip_trans)
#м    print(alpha,beta,gamma)
    return flip_tot


def redraw_pcd(alpha, beta, gamma, scale):
    global pcd_h_read
    global tkimage
    global rotated_cl
    global unrotated_cl
    global info_col
    global scanning
    global autoclickpt
    global colormap
    global rotcenter

    blank_image = np.zeros((y_height, x_width, 3), np.uint8)

    rotated_cl = []
    unrotated_cl = []

    (Dx, Dy, Dz) = rotcenter

    rotation_tot = make_transform(alpha, beta, gamma, Dx, Dy, Dz)

    for idx,trio in enumerate(scanning):
#        line = np.array([trio[0], trio[1], trio[2], trio[3]])
        line = np.array([trio[0] - Dx, trio[1] - Dy, trio[2] - Dz, trio[3]])
        rotated = np.dot(line, rotation_tot)


        (x, y, z, i_light) = rotated

        yz = int(y_height / 2 - y_height * y / scale / 2)
        xz = int(x_width * x / scale / 2 + x_width / 2)

        if xz <= 0:
            continue
        if yz <= 0:
            continue

        if xz >= x_width:
            continue
        if yz >= y_height:
            continue

        blank_image[yz][xz] = (255*(8-z)/7,255*(8-z)/7,255)

        # закинули в словарики для быстрого анализа клика
        rotated_cl.append((xz, yz, z))
        unrotated_cl.append(idx)

    pclimg = Image.fromarray(blank_image)
    tkimage = ImageTk.PhotoImage(image=pclimg)
    w.delete(all)
    w.create_image(0, 0, image=tkimage, anchor="nw")
    w.update()

    # draw points found

    for lines in marks:
        (xz, yz, zz, intensiv) = lines[2]
#        line = np.array([xz, yz, zz, intensiv])
        line = np.array([xz-Dx, yz-Dy, zz-Dz, intensiv])
        rotated = np.dot(line, rotation_tot)
        (x, y, z, i_light) = rotated

        #        if z <= 0:
        #            continue  # I haven't eye on the nape

        yz = int(y_height / 2 - y_height * y / scale / 2)
        xz = int(x_width * x / scale / 2 + x_width / 2)

        if xz <= 0:
            continue
        if yz <= 0:
            continue

        if xz >= x_width:
            continue
        if yz >= y_height:
            continue
        if lines[0] == autoclickpt:
            w.create_oval(xz - 5, yz - 5, xz + 5, yz + 5, fill="#00FF00")
        else:
            w.create_oval(xz - 5, yz - 5, xz + 5, yz + 5, fill="#FF6042")

    return


def button_rot_left():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_y > -np.pi:
        rotangle[1] -= rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_up():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_x < np.pi:
        rotangle[0] += rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_down():
    """rot down"""
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_x > -np.pi:
        rotangle[0] -= rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_rot_right():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if angle_y < np.pi:
        rotangle[1] += rot_step
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_scale_inc():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle
    if scale < 1:
        scale *= 2

    elif scale < 20:
        rotangle[3] += 1
    redraw_pcd(angle_x, angle_y, angle_z, scale)
    return


def button_rot_z(direction):
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if abs(angle_z) <= np.pi:
        rotangle[2] += int(direction) * rot_step / 2.
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    return


def button_scale_dec():
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle

    if scale > 1:
        rotangle[3] -= 1
    else:
        rotangle[3] /= 2.

    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])


def button_clear(mode):
    global rotangle

    if mode == 0:
        rotangle = [0, 0, 0, 8]
        redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])

    if mode == 1:
        rotangle = [np.pi / 2., 0, 0, 9]
        redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])


def load_filePCD(mode=1):
    global pcd_h_read

    global rotangle
    global scanning
    global colormap
    global defaultpcdfile
    global voxelfactor

    scanning.clear()

    if mode == 1:
        fname = askopenfilename(initialdir=input_path, filetypes=(("PCD", "*.pcd"), ("All files", "*.*")))
    else:
        fname = defaultpcdfile

    pcd_h_read = lib3d.read_point_cloud(fname)
    pcd_h_read = lib3d.voxel_down_sample(pcd_h_read, voxel_size=float(voxelfactor))
    #lib3d.draw_geometries([pcd_h_read])


    num_read = np.asarray(pcd_h_read.points)
    #colormap = np.asarray(pcd_h_read.colors)

    test = []
    for idx,(x,y,z) in enumerate(num_read):
        scanning.append([x, y, z, 0])
        test.append([x, y, z])
    #pcd = lib3d.PointCloud()
    #pcd.points = lib3d.Vector3dVector(test)
    #lib3d.draw_geometries([pcd])

    print(str(len(scanning)) + " points read")
    redraw_pcd(rotangle[0], rotangle[1], rotangle[2], rotangle[3])
    autobox(1)
    return


def click_analyze(event):
    """
    trying to find something near mouse click

    :param event:
    """
    # координаты клика
    global rotated_cl
    global rotangle
    (angle_x, angle_y, angle_z, scale) = rotangle
    global marks
    global flag_markstate
    global info_col
    global global_mode

    delta = 2  # area of search delta x delta points

    x, y = (event.x), (event.y)

    # trying to find something testy
    for idx, (sx, sy, sz) in enumerate(rotated_cl):

        if abs(x - sx) < delta and abs(y - sy) < delta:
            position = unrotated_cl[idx]
            if global_mode == None:
                scanning[position][3] = 2  # mark points
                marks.append([x, y, scanning[position], None, None])

                flag_markstate = 1
                info_col.set("Please select pair point on image panel")
            if global_mode == 'wait_click':
                autobox(2, position)
            break
    (angle_x, angle_y, angle_z, scale) = rotangle
    redraw_pcd(angle_x, angle_y, angle_z, scale)
    return


def ReadConfigfile():
    global descsize
    global defaultpcdfile
    global XAnc
    global YAnc
    global ZAnc
    global RotAnc
    global TiltAnc
    global voxelfactor
    global input_path
    global output_path

    config = configparser.ConfigParser()
    config.read('configXYZ.ini')

    #CameraMatrix2 = json.loads(config['DEFAULT']['CameraMatrix'])
    descsize = json.loads(config['DEFAULT']['CalibrationDescSize'])
    defaultpcdfile = config['DEFAULT']['DefaultPcdFile']
    XAnc.set(config['DEFAULT']['XAnc'])
    YAnc.set(config['DEFAULT']['YAnc'])
    ZAnc.set(config['DEFAULT']['ZAnc'])
    RotAnc.set(config['DEFAULT']['RotAnc'])
    TiltAnc.set(config['DEFAULT']['TiltAnc'])
    TiltAnc.set(config['DEFAULT']['TiltAnc'])
    voxelfactor = config['DEFAULT']['VoxelFactor']

    input_path = config['DEFAULT']['InputPath']
    output_path = config['DEFAULT']['OutputPath']

    return


def main():

    master = Tk()
    master.resizable(0, 0)

    global w
    global w1
    global check_reproj_var
    global XAnc
    XAnc = StringVar()
    XAnc.set("0")

    global YAnc
    YAnc = StringVar()
    YAnc.set("0")

    global ZAnc
    ZAnc = StringVar()
    ZAnc.set("0")

    global RotAnc
    RotAnc = StringVar()
    RotAnc.set("0")

    global TiltAnc
    TiltAnc = StringVar()
    TiltAnc.set("0")


    check_reproj_var = IntVar()
    check_reproj_var.set(1)
    ReadConfigfile()

    frame1 = Frame(master, relief=RAISED, borderwidth=1)
    frame1.pack(fill=BOTH, expand=True, side=TOP)
    frame3 = Frame(master, relief=RAISED, borderwidth=1)
    frame3.pack(fill=BOTH, expand=True, side=TOP)

    frame2 = Frame(master, relief=RAISED, borderwidth=1)
    frame2.pack(fill=BOTH, expand=True, side=TOP)

    frame4 = Frame(master, relief=RAISED, borderwidth=1)
    frame4.pack(fill=BOTH, expand=True, side=TOP)

    Label(frame1, text="PCL control:").pack(anchor=W, side=LEFT)
    Button(frame1, text="<-", bg="blue", fg='white', command=button_rot_left, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="^", bg="blue", fg='white', command=button_rot_up, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="v", bg="blue", fg='white', command=button_rot_down, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="->", bg="blue", fg='white', command=button_rot_right, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="RotZ R", bg="blue", fg='white', command=lambda: button_rot_z(1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="RotZ L", bg="blue", fg='white', command=lambda: button_rot_z(-1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)

    Button(frame1, text="-", bg="green", fg='white', command=button_scale_inc, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="+", bg="green", fg='white', command=button_scale_dec, repeatdelay=500, repeatinterval=10).pack(
        anchor=W, side=LEFT)
    Button(frame1, text="Top View", bg="red", fg='white', command=lambda: button_clear(1), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)
    Button(frame1, text="Z View", bg="red", fg='white', command=lambda: button_clear(0), repeatdelay=500,
           repeatinterval=10).pack(anchor=W, side=LEFT)

    w = Canvas(frame2, width=1280, height=800)
    w.pack(expand=YES, fill=BOTH, side=TOP)
    w.create_line(0, 0, 200, 100)
    w.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
    w.bind("<Button-1>", click_analyze)



    global r_var
    r_var = IntVar()
    r_var.set(1)

    global info_col
    info_col = StringVar()
    info_col.set("Please load files first")



    #    Button(frame3, text= "Process", bg = "green", command = button_apply2 ).pack(anchor=W, side = LEFT)
    Label(frame3, text="From Robot Shift: X:").pack(anchor=W, side=LEFT)
    Entry(frame3, width=12, textvariable=XAnc).pack(anchor=W, side=LEFT)
    Label(frame3, text="Y:").pack(anchor=W, side=LEFT)
    Entry(frame3, width=12, textvariable=YAnc).pack(anchor=W, side=LEFT)
    Label(frame3, text="Z:").pack(anchor=W, side=LEFT)
    Entry(frame3, width=12, textvariable=ZAnc).pack(anchor=W, side=LEFT)
    Label(frame3, text="Rot:").pack(anchor=W, side=LEFT)
    Entry(frame3, width=12, textvariable=RotAnc).pack(anchor=W, side=LEFT)
    Label(frame3, text="Tilt:").pack(anchor=W, side=LEFT)
    Entry(frame3, width=12, textvariable=TiltAnc).pack(anchor=W, side=LEFT)

    Label(frame3, textvariable=info_col).pack(anchor=W, side=LEFT)
    Button(frame3, text="open PCD file", command=load_filePCD, width=10).pack(anchor=N, side=RIGHT)
    Button(frame3, text="fast.pcd", command=lambda: load_filePCD(0), width=10).pack(anchor=N, side=RIGHT)

    global XAncRes
    XAncRes = StringVar()
    XAncRes.set("-")

    global YAncRes
    YAncRes = StringVar()
    YAncRes.set("-")

    global ZAncRes
    ZAncRes = StringVar()
    ZAncRes.set("-")

    global RotRes
    RotRes = StringVar()
    RotRes.set("-")

    global TiltRes
    TiltRes = StringVar()
    TiltRes.set("-")

    Label(frame4, text="ResultX:").pack(anchor=W, side=LEFT)
    Entry(frame4, width=12, textvariable=XAncRes).pack(anchor=W, side=LEFT)
    Label(frame4, text="Y:").pack(anchor=W, side=LEFT)
    Entry(frame4, width=12, textvariable=YAncRes).pack(anchor=W, side=LEFT)
    Label(frame4, text="Z:").pack(anchor=W, side=LEFT)
    Entry(frame4, width=12, textvariable=ZAncRes).pack(anchor=W, side=LEFT)
    Label(frame4, text="Rot:").pack(anchor=W, side=LEFT)
    Entry(frame4, width=12, textvariable=RotRes).pack(anchor=W, side=LEFT)
    Label(frame4, text="Tilt:").pack(anchor=W, side=LEFT)
    Entry(frame4, width=12, textvariable=TiltRes).pack(anchor=W, side=LEFT)

    button_clear(0)
    master.mainloop()


if __name__ == "__main__":
    main()

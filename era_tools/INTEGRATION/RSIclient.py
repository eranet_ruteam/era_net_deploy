"""
Author: Alex Kolker (fiery77@yandex.ru)
Date: Sep 30, 2019
Cервер-Клиент, котороый каждые 4 мс отвечает на запрос Куки, ожидает от Жанны в треде  
receiving_data_from_vision(ip, port) по UDP и отправлят назад шаблонизированный ответ
Server-Client for 4ms Kuka RSI communication with separate thread Vision subsystem/
"""


import socket
import configparser
import xml.etree.ElementTree as ET
import time
import pickle
from threading import Thread
from jinja2 import Template

dof_position = [0,0,0,0,0,0,0]
dof_names = ['XX','YY','ZZ','AA','BB','CC','TT']
# XX,YY,ZZ,AA,BB,CC - dof
# TT - timestamp (in ms) of last 6dof estimation


def initialize_socket(ipaddr, port, timeout = 10):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("client start" + str((ipaddr, port)))
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.settimeout(timeout)
    s.bind((ipaddr, port))
    return s

def ReadConfigfile():
    settings = {}

    config = configparser.ConfigParser()
    config.read('./config.ini')

    settings['SERVER_IP'] = config['DEFAULT']['SERVER_IP']
    settings['XML_FILE_NAME'] = config['DEFAULT']['XML_FILE_NAME']
    settings['BUFFER_SIZE'] = int(config['DEFAULT']['BUFFER_SIZE'])
    settings['SERVER_PORT'] = int(config['DEFAULT']['SERVER_PORT'])
    settings['SERVER_IP'] = config['DEFAULT']['SERVER_IP']
    settings['CLIENT_PORT'] = int(config['DEFAULT']['CLIENT_PORT'])
    settings['CLIENT_IP'] = config['DEFAULT']['CLIENT_IP']
    settings['VISION_IP'] = config['DEFAULT']['VISION_IP']
    settings['VISION_PORT'] = int(config['DEFAULT']['VISION_PORT'])
    return settings


def receiving_data_from_vision(ip, port):
    global dof_position
    #wating for a 3 secs for vision subsystem answering
    sock = initialize_socket(ip, port, 3)
    while True:
        try:
            input_text = sock.recv(1520)
            try:
                dof_position = pickle.loads(input_text)
                print("vision has ansvered"+str(type(dof_position[0])))
            except:
                print(" message load error")
                dof_position = [0,0,0,0,0,0,0]
        except socket.timeout:
            print("timeout")
            dof_position = [0,0,0,0,0,0,0]
        time.sleep(0.1)


def run_client():

    global dof_position
    global dof_names
    templ = open("./templatersi.templ").read()


    settings = ReadConfigfile()
# start child process for wating data from vision subsystem
    thread = Thread(target=receiving_data_from_vision,args=('', settings['CLIENT_PORT']))
    thread.start()

if __name__ == "__main__":
    run_client()

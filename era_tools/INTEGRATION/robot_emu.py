"""
Author: Alex Kolker (fiery77@yandex.ru)
Date: Sep 30, 2019

"""


import socket
import configparser
import xml.etree.ElementTree as ET
import  time


def ReadConfigfile():
    settings = {}

    config = configparser.ConfigParser()
    config.read('./config.ini')

    settings['SERVER_IP'] = config['DEFAULT']['SERVER_IP']
    settings['XML_FILE_NAME'] = config['DEFAULT']['XML_FILE_NAME']
    settings['BUFFER_SIZE'] = int(config['DEFAULT']['BUFFER_SIZE'])
    settings['SERVER_PORT'] = int(config['DEFAULT']['SERVER_PORT'])
    settings['SERVER_IP'] = config['DEFAULT']['SERVER_IP']
    settings['CLIENT_PORT'] = int(config['DEFAULT']['CLIENT_PORT'])
    settings['CLIENT_IP'] = config['DEFAULT']['CLIENT_IP']
    return settings

def initialize_socket(settings):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.settimeout(2)
    print("server start:"+str((settings['SERVER_IP'], settings['SERVER_PORT'])))
    print("wating for a client:"+str((settings['CLIENT_IP'], settings['CLIENT_PORT'])))
    s.bind((settings['SERVER_IP'], settings['SERVER_PORT']))
    return s


def main():
    settings = ReadConfigfile()
    sock = initialize_socket(settings)
    f = open(settings['XML_FILE_NAME'], 'r')
    buffer = f.read()


    while True:
        try:
            result = sock.sendto(buffer.encode(),(settings['CLIENT_IP'],settings['CLIENT_PORT']))
            data, address = sock.recvfrom(settings['BUFFER_SIZE'])
            parser = ET.fromstring(data)
            for child in parser:
                print(child.tag,child.text, child.attrib)

        except socket.timeout:
            print("Didn't receive data! [Timeout 2s]")
            continue

        time.sleep(1)


if __name__ == "__main__":
    main()

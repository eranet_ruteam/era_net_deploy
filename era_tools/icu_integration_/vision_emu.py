"""
Author: Alex Kolker (fiery77@yandex.ru)
Date: Oct 1, 2019

Этот код должен появиться в конце программы Жанна для коммуникации с RSI сервер-клиентом
This code should be integrated into Vision subsystem
"""


import socket
import time
import pickle
import configparser

def initialize_socket(ipaddr, port, timeout = 1):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("client start" + str((ipaddr, port)))
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.settimeout(timeout)
    s.bind((ipaddr, port))
    return s


def main():
    six_dof = [0,0,0,0,0,0]

    config = configparser.ConfigParser()
    config.read('./config.ini')
    sock = initialize_socket('127.0.0.1',5009,1)

    while True:
        six_dof_w_time = []
        six_dof_w_time = six_dof.copy()
        six_dof_w_time.append(int(time.time()*1000))
        print("Send 6dof w time"+str(six_dof_w_time))
        sock.sendto(pickle.dumps(six_dof_w_time),(config['DEFAULT']['VISION_IP'],int(config['DEFAULT']['VISION_PORT'])))
        time.sleep(0.1)



if __name__ == "__main__":
    main()


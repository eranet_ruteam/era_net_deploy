﻿The tool for artificial image generation howto


The tool set was developed for generation high-quality coco-style synthetic dataset for neural networks training. 


1. Related works
https://github.com/hanskrupakar/COCO-Style-Dataset-Generator-GUI


A simple GUI-based COCO-style JSON Polygon masks' annotation tool to facilitate quick and efficient crowd-sourced generation of annotation masks and bounding boxes. Optionally, one could choose to use a pretrained Mask RCNN model to come up with initial segmentations. 


Could be used for hand-made annotation existing images.


2.
https://github.com/andytung2019/COCO-Style-Dataset-Generator-GUI


A simple GUI-based COCO-style JSON Polygon masks' annotation tool to facilitate quick and efficient crowd-sourced generation of annotation masks and bounding boxes. Optionally, one could choose to use a pretrained Mask RCNN model to come up with initial segmentations. 


3. Medical care
@misc{oleks2019red,
    title={Red blood cell image generation for data augmentation using Conditional Generative Adversarial Networks},
    author={Oleksandr Bailo and DongShik Ham and Young Min Shin},
    year={2019},
    eprint={1901.06219},
    archivePrefix={arXiv},
    primaryClass={cs.CV}
}




4. https://blog.aimultiple.com/synthetic-data/ The post is discussing the benefits of synthetic data generation for NN training




BizDataX
	2005
	Private
	11-50
	CA Technologies Datamaker
	1976
	Public
	10,001+
	CVEDIA
	2016
	Private
	11-50
	Deep Vision Data by Kinetic Vision
	1985
	Private
	51-200
	Delphix Test Data Management
	2008
	Private
	201-500
	Genrocket
	2012
	Private
	11-50
	Informatica Test Data Management Tool
	1993
	Private
	1,001-5,000
	Neuromation
	2016
	Private
	11-50
	Solix EDMS
	2002
	Private
	51-200
	Supervisely
	2017
	Private
	2-10
	TwentyBN
	2015
	Private
	11-50
	

5. Some researchers have thought about using tracers to create training databases. 
https://www.doc.ic.ac.uk/~ajd/Publications/McCormac-J-2019-PhD-Thesis.pdf
However, a tool that could create high-quality annotated sets of multiple overlapped objects has not yet realized.






6. NVIDIA Deep learning Dataset Synthesizer (NDDS) a  UE4 plugin from NVIDIA to empower computer vision researchers to export high-quality synthetic images with metadata. NDDS supports images, segmentation, depth, object pose, bounding box, keypoints, and custom stencils. In addition to the exporter, the plugin includes different components for generating highly randomized images. This randomization includes lighting, objects, camera position, poses, textures, and distractors, as well as camera path following, and so forth. Together, these components allow researchers to easily create randomized scenes for training deep neural networks.
The strong features of Nvida tool:
* the ability of using physical engine
* the flexible GUI-based  basic scene configuring
* ability if using coloured meshes and RGBD pointcliuds; 
The main weak features of Nvidia tool:
* UE4 dependence ( CUDA and graphics need);
* batch mode is problematic;
* external scene configuration is complicated for realisation.
*    






The current toll advantages and difference from existing projects


We would like to present the tool which is able to generate synthetic data set for batch of objects in automatic mode.


Thee tool based on http://www.povray.org/: the Persistence of Vision Raytracer is a high-quality, Free Software tool for creating stunning three-dimensional graphics. The source code is available for those wanting to do their own ports.


The main target of current project is developing python based tool for making artificial images from mesh models which could be easily implemented into self learning processing. All instances should be easelly configured by using text-based config files. The tool could be used without smart packet dependencies. 
        
Data set pre configuration
1. Preparation of an object model. The object model should be represented as an obj mesh format. Angle of rotation [0,0,0] - view from the camera to the object. You can use Blender © tool for rotating object to desirable [0,0,0]  initial position. The shift relative to the origin does not matter - the part will be automatically centered in the center of its bounding box


2. The recommended unit dimension in the obj coordinate fields is millimeters (unless otherwise indicated). It should be borne in mind that for parts with dimensions greater than 500mm, you may need to adjust the intensity of the brightness of the lighting (this will be necessary in the templates for pov files)


The current project limitation:


The current version does not use a physical engine to estimate the position of objects  in accordance with physics and gravity: this is planned to be done in the future. The connection-gate with a physics could be made is initial position of details (zero position).  Also, the current version of the software implements the generation of metal objects with a matt zinc-aluminum surface. The material could be changed in povray template file if it is neccesary. 




Data set pre configuration
1. Preparation of an object model. The object model should be represented as an obj mesh format. Angle of rotation [0,0,0] - view from the camera to the object. You can use Blender © tool for rotating object to desirable [0,0,0]  initial position. The shift relative to the origin does not matter - the part will be automatically centered in the center of its bounding box


2. The recommended unit dimension in the obj coordinate fields is millimeters (unless otherwise indicated). It should be borne in mind that for parts with dimensions greater than 500mm, you may need to adjust the intensity of the brightness of the lighting (this will be necessary in the templates for pov files)


/ The project files stricture


/input
/result/
        images/
        annotations/
        backgrounds/
                out/
                cutvideo.sh
                video.avi
/total/result
                /annotatons
                /images
                /imagesbg
./povray2




genpovray.py
genpovray.ini
start_generation.sh


--------- Config file description


[DEFAULT]


# Main light  positions. 


#There are 4 additional light source which provides background lightening - they does not allowed to change


LIGHT = [[0, 0, 0], [0, -40, 0], [0, 40, 0], [-20, -20, 0], [20, 20, 0]]






#Details and annotation data. Rotation 0,0,0 - view from camera to detail.


OBJECTS = [["./input/bolt.obj", "BOLT", 0], ["./input/bow1.obj", "bow", 1], ["./input/lanya.obj




SCALE = 1




In the configuration file, the user can change the rotation of the position of the light source (in degrees) [[rot_x, rot_y, rot_z]],
stack of parts
[[“Part file name”, “part class name”, int (part class number)]]


The SCALE parameter is responsible for the multiplier of the distance the camera is removed from the position of the part. The distance of the camera from the part is calculated by using equiation:






camera_position = max(boundiary_box_size) * 1.7 * SCALE






therefore if SCALE > 1 then camera became farest from detail, < 1 - closest. 




___ Starting of generation


To start data generation, you need to specify the number of sets (total number of images = number of sets * number of light positions) start index and configuration file.




python3 genpovray.py 75 0  1 config.ini&
                                      \   \   \    \
                                       \   \   \     ---------- config file
                                       \   \    -------------- 1 — clear folders, 0 — continue generation
                                       \   ------------------start index for coco generation
                                       ---------------------The number of images to be created




The start index is necessary if the data generation was interrupted and it is necessary to continue it from the specified index (critical for creating the json description file) or for parallel generation from several sessions with the possibility of final combining of samples.


The created image files with a transparent background and the corresponding description files will be created in the ./result/images, ./result/annotations directories, the directories will be cleared if the corresponding command line parameter is specified.






The image size is fixed and equal 704 by 704 pixels. Based on this resolution, the camera position will automatically calculated. Parts will sequentially overlapped on the images (the part with the index 0 is the lowest, the part with the highest index is the highest).


Parts will receive a random relative position within their bounding rectangles in x, y and a fixed distance z so as to avoid overlapping.


Parts will receive random rotation respecting the z axis (the x axis is horizontal, the y axis is vertical, the z axis is from the camera to the observer.




___  Background substitution


During the generation process, the parts will be created with a transparent background and located in the image directory. An arbitrary video cut into frames is used to substitute a random background. The script ./result/background/cutvideo.sh performs slicing into frames of the required dimension; the video slicing is performed ./result/background/video.avi


Steven Spielberg's Home 2009 film was chosen as the source of the video due to open access copiright. 




Home is a 2009 French documentary film by Yann Arthus-Bertrand. The film is almost entirely composed of aerial shots of various places on Earth. It shows the diversity of life on Earth and how humanity is threatening the ecological balance of the planet. ann Arthus-Bertrand said in a TED talk[11] that the movie has no copyright: "This film have [sic] no copyright. On the fifth of June, the environmental day, everyone can download the movie on Internet. The film is given for free by the distributor to TV and theater to show it on June 5th." Nevertheless, a copyright notice appears in the final credits. 


Using this video is especially symbolic. Convolutional neural networks are currently gaining an increasingly prominent role in our lives. At the same time, NN processing  is still a very energy-consuming process. Researchers who being  working in this areal have to go a long way to the moment when these mathematical algorithms become effective.




Running the cutvideo.sh script will prepare 2,600 images, which will then be randomly used by the ./rebackground.sh script to substitute the background. The resulting images will be copied to the total / result / images folders and their description files will be placed there (result / annotation)


You can continue processing if need.




The result of the program running. As a result of the program performing, the user receives:
* annotations as separate json files in the ./result/annotaions directory. Each contour of the part corresponds to one file in the directory.
* images with a transparent background in the ./result/images catalog;
* images with a substituted background in the ./result/imagesbg catalog;
* joint annotation ./result.annotation.json, which refers to files in the ./result/imagesbg directory which could be directly used for NN train.

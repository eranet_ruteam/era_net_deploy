#!/usr/bin/python
#

import numpy as np
import cv2
import os
import sys


def overlay_transparent(background_img, img_to_overlay_t, x, y, overlay_size=None):
    """
    @brief      Overlays a transparant PNG onto another image using CV2

    @param      background_img    The background image
    @param      img_to_overlay_t  The transparent image to overlay (has alpha channel)
    @param      x                 x location to place the top-left corner of our overlay
    @param      y                 y location to place the top-left corner of our overlay
    @param      overlay_size      The size to scale our overlay to (tuple), no scaling if None

    @return     Background image with overlay on top
    """

    bg_img = background_img.copy()

    if overlay_size is not None:
        img_to_overlay_t = cv2.resize(img_to_overlay_t.copy(), overlay_size)

    # Extract the alpha mask of the RGBA image, convert to RGB
    b, g, r, a = cv2.split(img_to_overlay_t)
    overlay_color = cv2.merge((b, g, r))

    # Apply some simple filtering to remove edge noise
    mask = cv2.medianBlur(a, 5)

    h, w, _ = overlay_color.shape
    roi = bg_img[y:y + h, x:x + w]

    # Black-out the area behind the logo in our original ROI
    img1_bg = cv2.bitwise_and(roi.copy(), roi.copy(), mask=cv2.bitwise_not(mask))

    # Mask out the logo from the logo image.
    img2_fg = cv2.bitwise_and(overlay_color, overlay_color, mask=mask)

    # Update the original image with our new ROI
    bg_img[y:y + h, x:x + w] = cv2.add(img1_bg, img2_fg)

    return bg_img



if len(sys.argv) != 4:
    stopN = 1
    print(
        "no command string:using default: ./dir_with_images  ./dir_with_backgrounds ./outdir -> ./result/images0 ./result/background/image ./result/images")
    dir_in1 = "./result/images0"
    background_dir = "./result/background/out"
    out_dir = "./result/images"

else:
    dir_in1 = sys.argv[1]
    background_dir = sys.argv[2]
    out_dir = sys.argv[3]
    print("background substitution {} + {} = {} ".format(dir_in1, background_dir, out_dir))

files = [f for f in os.listdir(dir_in1) if f.endswith('.png')]
backgrounds = [f for f in os.listdir(background_dir) if f.endswith('.png')]
if len(files) > 0 and len(backgrounds) > 0:
    print("{} images and {} backgrounds being processed".format(len(files), len(backgrounds)))
    for idx in range(0, len(files)):
        print(files[idx])
        overlay = cv2.imread(dir_in1 + '/' + files[idx], -1)
        background_img = cv2.imread(background_dir + '/' + backgrounds[np.random.randint(1, len(backgrounds), 1)[0]])

        cv2.imwrite(out_dir + '/' + files[idx], overlay_transparent(background_img, overlay, 0, 0, (704, 704)))

//wrap the file with the version
#local Temp_version = version;
#version 3.7;

 
global_settings {
  //This setting is for alpha transparency to work properly.
  //Increase by a small amount if transparent areas appear dark.
   max_trace_level 15
   adc_bailout 0.01
   assumed_gamma 1
   ambient_light rgb <1,1,1>*0
 
}
#include "detail0_POV_geom_.inc" 
#include "detail1_POV_geom_.inc" 
#include "detail2_POV_geom_.inc" 

#include "metal_custom.inc"
#include "colors.inc"
#include "metals.inc"

 
//CAMERA PoseRayCAMERA
//CAMERA PoseRayCAMERA
camera {
        perspective
        right -x*image_width/image_height
        location <0,0,606.6532742>
        look_at 0
        rotate <0,0,0>
        rotate <0,-42,0>
        rotate <0,0,0>

        }

 
//PoseRay default Light attached to the camera
light_source {
              <0,0,700> //light position
              color rgb <1,1,1>*1.7
              rotate <20,0,0> //roll
              rotate <0,20 ,0> //pitch
              rotate <0,0,0> //yaw
             }


//background lighting
light_source {
              <0,0,2000> //light position
              color rgb <0.4,0.4,0.3>
              rotate <75,0,0> //roll
              rotate <0,0,0> //pitch
              rotate <0,0,0> //yaw
             }

light_source {
              <0,0,2000> //light position
              color rgb <0.4,0.4,0.3>
              rotate <-75,0,0> //roll
              rotate <0,0,0> //pitch
              rotate <0,0,0> //yaw
             }


light_source {
              <0,0,2000> //light position
              color rgb <0.4,0.4,0.3>
              rotate <0,0,0> //roll
              rotate <0,75,0> //pitch
              rotate <0,0,0> //yaw
             }

light_source {
              <0,0,2000> //light position
              color rgb <0.4,0.4,0.3>
              rotate <0,0,0> //roll
              rotate <0,-75,0> //pitch
              rotate <0,0,0> //yaw
             }


//Background
background { color srgbt <0,0,0,1>  }

object{
      detail0

            translate <140.8,-140.8,-177>
            rotate <0,0,0> //roll
	    rotate <0,0,0> //pitch
    	    rotate <0,0,45> //yaw

      texture { pigment { color rgb <0.64,0.687,0.71>  }

	        finish{F_MetalD}
                normal  { agate 0.1 scale 0.1 }}

      }

//  plane { <0,-1,0> // normal vector
//        , 500 // distance from origin
//    pigment {
//      checker color , color Black, scale 150
//    }
//  }

//  plane { <0,1,0> // normal vector
//        , 500 // distance from origin
//    pigment {
//      checker color White*0.05, color Black, scale  150
//    }
//  }


object{
      detail1

            translate <-140.8,140.8,140.99032599999998>
            rotate <0,0,0> //roll
	    rotate <0,0,0> //pitch
    	    rotate <0,0,-87> //yaw

      texture { pigment { color rgb <0.64,0.687,0.71>  }

	        finish{F_MetalD}
                normal  { agate 0.1 scale 0.1 }}

      }

//  plane { <0,-1,0> // normal vector
//        , 500 // distance from origin
//    pigment {
//      checker color , color Black, scale 150
//    }
//  }

//  plane { <0,1,0> // normal vector
//        , 500 // distance from origin
//    pigment {
//      checker color White*0.05, color Black, scale  150
//    }
//  }


object{
      detail2

            translate <0,0,-146.170843>
            rotate <0,0,0> //roll
	    rotate <0,0,0> //pitch
    	    rotate <0,0,47> //yaw

      texture { pigment { color rgb <0.64,0.687,0.71>  }

	        finish{F_MetalD}
                normal  { agate 0.1 scale 0.1 }}

      }

//  plane { <0,-1,0> // normal vector
//        , 500 // distance from origin
//    pigment {
//      checker color , color Black, scale 150
//    }
//  }

//  plane { <0,1,0> // normal vector
//        , 500 // distance from origin
//    pigment {
//      checker color White*0.05, color Black, scale  150
//    }
//  }


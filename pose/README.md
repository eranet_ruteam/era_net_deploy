### Get docker image: ###

* Version with visualization for images is 1.2.1:
`docker pull eraneticu/posecnn:1.2.1`
* Version without visualization for images is 1.2:
`docker pull eraneticu/posecnn:1.2`
* Version with visualization for videos is 1.3:
`docker pull eraneticu/posecnn:1.3`
* Version with visualization for webcam is 1.4:
`docker pull eraneticu/posecnn:1.4`

[docker hub](https://hub.docker.com/r/eraneticu/posecnn/)

**Make data directory structure**

`sh ./paths.sh` 

from project root directory

### Training: (not succesfull on mobile GTX): ###
** Source Pose code directory for training is `era_net_deploy/pose/src/PoseCNN` **

* Put data images into 
    
`data/input_training/data/`
    
* Put .xyz (Open a model of object (.ply) in CloudCompare and save it in .XYZ format) or download [example](https://drive.google.com/file/d/1bK2FsnqAVhOHGkOLS57xmSFXldOAnh14/view?usp=sharing) to

`data/input_training/models/`

* Run `run_training.sh` output should be in 

`data/output_training`



### Testing: ###
** Source Pose code directory for training is `era_net_deploy/pose/src/PoseCNN_img_test` **

* After training steps, weights can be available ib

`/data/outout_training/`

* If you split test video into frames (image tests) copy them to

`data/input_testing/images`

* Or place video file (video tests) to

`data/input_testing/videos`

* Put .xyz file to

`data/input_testing/models/`


* For camera choice edit file docker-compose-webcam.yml, change command: "./experiments/scripts/demo.sh 0 cam0" to command: "./experiments/scripts/demo.sh 0 camN" where N is camera id. 
Also change `devices:
            - /dev/video0`
To `devices:
            - /dev/videoN`

* Camera Distorsion Matrix store in the `./config/camera.ini` (initially copy `camera.ini.tmp` to `camera.ini` and set required values)

* For image tests without visualization run `cd yml-dev && docker-compose --file docker-compose-testing.yml up`
* **For image tests with visualization run `run_testing.sh` or `cd yml-dev && docker-compose --file docker-compose-testing-vis.yml up`**
* For video file tests run `cd yml-dev && docker-compose --file docker-compose-video.yml up`
* For webcam tests run `cd yml-dev && ./run_webcam.sh` or `cd yml-dev && docker-compose --file docker-compose-webcam.yml up`
* **For ZED camera tests run `./run_ZED.sh` or `docker-compose --file docker-compose-zed.yml up` and change file `./config/camera.ini` to appropriate camera intrinsic parameters**


[google disk link](https://drive.google.com/drive/folders/18ZBgoW0Z1t7v6OgO8AlwvpRQsz_mW5Hh)

if [ $# -eq 0 ]
  then
    echo 'Use `build.sh VERSION`'
    exit
fi

nvidia-docker build -t eraneticu/posecnn -t eraneticu/posecnn:$1 .
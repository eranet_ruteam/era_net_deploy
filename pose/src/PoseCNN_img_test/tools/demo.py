#!/usr/bin/env python

# --------------------------------------------------------
# FCN
# Copyright (c) 2016 RSE at UW
# Licensed under The MIT License [see LICENSE for details]
# Written by Yu Xiang
# --------------------------------------------------------

"""Test a FCN on an image database."""

import _init_paths
from fcn.test import test_net_images, im_segment_single_frame
from fcn.config import cfg, cfg_from_file
from datasets.factory import get_imdb
import argparse
import pprint
import time, os, sys
import tensorflow as tf
import os.path as osp
import numpy as np
import cv2
from utils.blob import im_list_to_blob, pad_im, unpad_im, add_noise
from utils.voxelizer import Voxelizer, set_axes_equal
from transforms3d.quaternions import quat2mat
from time import time
import json
import configparser

from render import render, load_ply


def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Test a Fast R-CNN network')
    parser.add_argument('--gpu', dest='gpu_id', help='GPU id to use',
                        default=0, type=int)
    parser.add_argument('--weights', dest='pretrained_model',
                        help='pretrained model',
                        default=None, type=str)
    parser.add_argument('--model', dest='model',
                        help='model to test',
                        default=None, type=str)
    parser.add_argument('--cfg', dest='cfg_file',
                        help='optional config file', default=None, type=str)
    parser.add_argument('--wait', dest='wait',
                        help='wait until net file exists',
                        default=True, type=bool)
    parser.add_argument('--imdb', dest='imdb_name',
                        help='dataset to test',
                        default='shapenet_scene_val', type=str)
    parser.add_argument('--network', dest='network_name',
                        help='name of the network',
                        default=None, type=str)
    parser.add_argument('--rig', dest='rig_name',
                        help='name of the camera rig file',
                        default=None, type=str)
    parser.add_argument('--cad', dest='cad_name',
                        help='name of the CAD file',
                        default=None, type=str)
    parser.add_argument('--kfusion', dest='kfusion',
                        help='run kinect fusion or not',
                        default=False, type=bool)
    parser.add_argument('--pose', dest='pose_name',
                        help='name of the pose files',
                        default=None, type=str)
    parser.add_argument('--background', dest='background_name',
                        help='name of the background file',
                        default=None, type=str)
    '''
    parser.add_argument('--video',
                        help='path to video or camera',
                        default=None, type=str)
    '''

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()

    print('Called with args:')
    print(args)

    if args.cfg_file is not None:
        cfg_from_file(args.cfg_file)

    print('Using config:')
    pprint.pprint(cfg)

    weights_filename = os.path.splitext(os.path.basename(args.model))[0]

    imdb = get_imdb(args.imdb_name)


    # construct meta data
    config = configparser.ConfigParser()
    config.read('/opt/config/camera.ini')
    CameraDistorsion = json.loads(config['DEFAULT']['CameraDistorsion'])
    K = np.array(CameraDistorsion)
    meta_data = dict({'intrinsic_matrix': K, 'factor_depth': 1.0})
    print meta_data

    cfg.GPU_ID = args.gpu_id
    device_name = '/gpu:{:d}'.format(args.gpu_id)
    print device_name

    cfg.TRAIN.NUM_STEPS = 1
    cfg.TRAIN.GRID_SIZE = cfg.TEST.GRID_SIZE
    cfg.TRAIN.TRAINABLE = False

    cfg.RIG = args.rig_name
    cfg.CAD = args.cad_name
    cfg.POSE = args.pose_name
    cfg.BACKGROUND = args.background_name
    cfg.IS_TRAIN = False

    from networks.factory import get_network
    network = get_network(args.network_name)
    print 'Use network `{:s}` in training'.format(args.network_name)

    # start a session
    saver = tf.train.Saver()
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, gpu_options=gpu_options))
    saver.restore(sess, args.model)
    print ('Loading model weights from {:s}').format(args.model)

    print imdb._points_all.shape
    # points = imdb._points_all[1]
    # x3d = np.ones((4, points.shape[0]), dtype=np.float32)
    # x3d[0, :] = points[:, 0]
    # x3d[1, :] = points[:, 1]
    # x3d[2, :] = points[:, 2]

    # construct the filenames
    root = 'data/demo_images/'
    names = os.listdir(root)
    
    names = list(filter(lambda x: x.find('color')!=-1, names))
    print names
    # print names
    num = [int(i.split('-color')[0]) for i in names if len(i.split('-color')) != 1]
    rgb_filenames = []
    depth_filenames = []
    
    folder_ply = '/opt/hinterstoisser/models'
    models = [load_ply(os.path.join(folder_ply, i)) for i in sorted(os.listdir(folder_ply))]
    im_size_rgb = (640, 480)
    clip_near = 10  # [mm]
    clip_far = 10000  # [mm]
    model_texture = None
    ambient_weight = 0.8  # Weight of ambient light [0, 1]
    shading = 'phong'
    surfs = [(1, 0, 0), (1, 1, 0), (0, 0, 1), (1, 0, 1)]
    voxelizer = Voxelizer(cfg.TEST.GRID_SIZE, imdb.num_classes)
    voxelizer.setup(-3, -3, -3, 3, 3, 4)

    '''
    if args.video[0:3] == 'cam':
        source = int(args.video[3:])
    else:
        source = args.video
    '''

    # cap = cv2.VideoCapture(source)
    # ok, image = cap.read()
    # while cap.isOpened():
    for i in names:
        filename = root + i # root + '{:04d}-color.png'.format(i)
        print filename
        # rgb_filenames.append(filename)
        # filename = root + '{:04d}-depth.png'.format(i)
        # print filename
        # depth_filenames.append(filename)

        # ok, image = cap.read()
        image = cv2.imread(filename, cv2.IMREAD_UNCHANGED)
        image = cv2.resize(image,(640,480))
        rgba = pad_im(image, 16)

        if rgba.shape[2] == 4:
            im = np.copy(rgba[:, :, :3])
            alpha = rgba[:, :, 3]
            I = np.where(alpha == 0)
            im[I[0], I[1], :] = 0
        else:
            im = rgba
        im_depth = np.zeros((im.shape[0], im.shape[1]), dtype=np.uint16)


        toc = time()
        labels, probs, vertex_pred, rois, poses = im_segment_single_frame(sess, network, im, im_depth, meta_data, voxelizer,
                                                                          imdb._extents, imdb._points_all,
                                                                          imdb._symmetry, imdb.num_classes)
        text_origin = [0, 15]
        smth = np.zeros((480, 640 ,3))
        for i in xrange(len(surfs)):
            smth[labels==i+1, :] = surfs[i]
        cv2.imshow('proval', smth)
	print poses.shape
        for i in xrange(poses.shape[0]):
            if int(rois[i, 1]) == 0: continue
            print 'model number '
            print int(rois[i, 1])-1
            print rois
            model = models[int(rois[i, 1])-1]
            RT = np.zeros((3, 4), dtype=np.float32)
            RT[:3, :3] = quat2mat(poses[i, :4])
            RT[:, 3] = poses[i, 4:7]

            rgb = render(model, im_size_rgb, K, RT[:3, :3], RT[:3, 3] * 1000, clip_near, clip_far,
                         texture=model_texture,
                         ambient_weight=ambient_weight, shading=shading, mode='rgb', surf_color=surfs[int(rois[i, 1])-1])
            tmp = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
            _, alpha = cv2.threshold(tmp, 0, 127, cv2.THRESH_BINARY)
            alpha = np.array([alpha] * 3)
            alpha = alpha.transpose((1, 2, 0))
            # print alpha.shape
            alpha = alpha.astype(float) / 255
            # print rgb.shape
            foreground = cv2.multiply(alpha, rgb.astype(float))
            background = cv2.multiply(1.0 - alpha, image.astype(float))
            image = np.uint8(cv2.add(foreground, background))

            # x2d = np.matmul(K, np.matmul(RT, x3d))
            # x2d[0, :] = np.divide(x2d[0, :], x2d[2, :])
            # x2d[1, :] = np.divide(x2d[1, :], x2d[2, :])
            #
            # for x, y in np.transpose(x2d[:2], (1, 0)):
            #     # print int(x), int(y)
            #     cv2.circle(image, (int(x), int(y)), 5, (0, 255, 0), -1)

            for line in RT:
                # line = np.array([float("{0:.4f}".format(n)) for n in line])
                # cv2.putText(image, str(line), tuple(text_origin), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 3)
                cv2.putText(image, str(line), tuple(text_origin), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 0), 1)
                text_origin[1] += 20
        tic = time()
        inference_time = tic-toc
        fps = 1/inference_time
        # cv2.putText(image, 'fps {:.1f}'.format(fps), (0, 470), cv2.FONT_HERSHEY_COMPLEX, 0.5, (0, 255, 0), 1)

        cv2.imshow('kek', image)
        if cv2.waitKey(0) == ord('q'):
            break

    # test_net_images(sess, network, imdb, weights_filename, rgb_filenames, depth_filenames, meta_data)

#!/usr/bin/env python3
# -*- coding: UTF-8 -*-


import numpy as np
#import open3d as lib3d

import pyrealsense2 as rs
import cv2


#
#
#

def main():

    # Create a pipeline
    pipeline = rs.pipeline()

    # Create a config and configure the pipeline to stream
    #  different resolutions of color and depth streams
    config = rs.config()
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 6)
    #config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 6)
    config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 6)

    # Start streaming
    profile = pipeline.start(config)

    # Getting the depth sensor's depth scale (see rs-align example for explanation)
    depth_sensor = profile.get_device().first_depth_sensor()
    depth_scale = depth_sensor.get_depth_scale()
    laser_pwr = depth_sensor.get_option(rs.option.laser_power)
    print("Depth Scale is: ", depth_scale)
    print("Laser power: ", laser_pwr)
    # We will be removing the background of objects more than
    #  clipping_distance_in_meters meters away
    #clipping_distance_in_meters = 1  # 1 meter
    #clipping_distance = clipping_distance_in_meters / depth_scale

    # Create an align object
    # rs.align allows us to perform alignment of depth frames to others frames
    # The "align_to" is the stream type to which we plan to align depth frames.
    align_to = rs.stream.color
    align = rs.align(align_to)

    # Skip 5 first frames to give the Auto-Exposure time to adjust
    for x in range(5):
        pipeline.wait_for_frames()

    # Store next frameset for later processing:
    frameset = pipeline.wait_for_frames()
    # Align the depth frame to color frame
    aligned_frames = align.process(frameset)

    # Get aligned frames
    aligned_depth_frame = aligned_frames.get_depth_frame()  # aligned_depth_frame
    color_frame = aligned_frames.get_color_frame()



    # Validate that both frames are valid
    if not aligned_depth_frame or not color_frame:
        print("error d+i")
        exit(0)

    color_image = np.asanyarray(color_frame.get_data())
    depth_image = np.asanyarray(aligned_depth_frame.get_data())
    cloud = []
#    for y,line in enumerate (depth_image):
#        for x,z in enumerate(line):
#            if 0 < z < 3000:
#                cloud.append([x, y, -z])
##            else:
#                cloud.append([x, y, None])


#    pcd_file = lib3d.PointCloud()
#    pcd_file.points = lib3d.Vector3dVector(cloud)
    print("finished")
#    lib3d.write_point_cloud("result.pcd", pcd_file)
    cv2.imwrite("../result/image.png",color_image)
    cv2.imwrite("../result/gray.png", depth_image)



    pipeline.stop()
    exit(1)

if __name__ == "__main__":
    main()

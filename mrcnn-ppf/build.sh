if [ $# -eq 0 ]
  then
    echo 'Use `build.sh VERSION`'
    exit
fi

nvidia-docker build -t eraneticu/mrcnn-ppf -t eraneticu/mrcnn-ppf:$1 .
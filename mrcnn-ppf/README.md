`___Ubuntu_18_04_docker_release___`

# Get latest docker image #

`docker pull eraneticu/mrcnn-ppf:latest`

[docker hub](https://hub.docker.com/r/eraneticu/mrcnn-ppf)

# Data directory structure #

**MRCNN-PPF:**

Weights, model and images data for test case have placed inside *data* directory. 

**Realsense:**

Realsense script is placed inside *src/scripts* directory and have mapped into */opt/scripts* docker directory

Realsense results is saved into *result* directory from */opt/result* docker directory

# Testing MRCNN-PPF: #

**`./run.sh`**

# Testing Realsense: #

**`./run-rs2.sh`**

# Rebuild docker: #

* `cd src`
* `git clone https://donuhr@bitbucket.org/eranet_ruteam/mrcnn-ppf.git`
* `git clone https://github.com/IntelRealSense/librealsense.git`
* `cd ../`
* `./build.sh [version]`
* `./push.sh`

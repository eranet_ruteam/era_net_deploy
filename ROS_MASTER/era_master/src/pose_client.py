"""
Author: Alexandr Gochakov (wandering@bk.ru)
Date: Mar 24, 2020
Клиент, котороый принимает данные от Жанны  
Сlient communication with separate Vision subsystem/
"""

import time
import json
import pickle
import rospy
import smach
from std_msgs.msg import String
import numpy as np

class Runner(smach.State):
    """
    Send Command to run Vision
    """
    def __init__(self, timeout=10):
        smach.State.__init__(self, outcomes=['vision'])

    def execute(self, userdata):
        pub = rospy.Publisher('statemachine', String, queue_size=10)
        rospy.loginfo("Hello from PUB node")
        r = rospy.Rate(10) # 10hz
        for i in range(3):
            pub.publish("vision")
            r.sleep()
        time.sleep(10)
        return 'vision'

class Pose(smach.State):
    """
    Get data from Vision
    """
    dof_names = ['XX', 'YY', 'ZZ', 'AA', 'BB', 'CC', 'TT']
    # XX,YY,ZZ,AA,BB,CC - dof
    # TT - timestamp (in ms) of last 6dof estimation

    def __init__(self, timeout=10):
        smach.State.__init__(self,
                             outcomes=['motion_planner', 'interruption'],
                             output_keys=['dof_object_out', 'dof_position_out'])
        self.timeout = timeout

    def vision_callback(self, data):
        rospy.loginfo(rospy.get_caller_id() + 'POSE result: %s\n', json.loads(data.data))
        self.dof_position = json.loads(data.data)

    def execute(self, userdata):
        #rospy.Subscriber('maskrcnn', String, self.vision_callback)
        while not rospy.is_shutdown():
            try:
                data = rospy.wait_for_message('maskrcnn', String, self.timeout)
                vision_input = json.loads(data.data)
                vision_objects = vision_input.keys()
                for obj in vision_objects:
                    pose_data = np.asarray(vision_input[obj])
                    userdata.dof_position_out = pose_data
                    userdata.dof_object_out = obj
                    return 'motion_planner'
            except rospy.exceptions.ROSException:
                return 'interruption'

    def stop(self):
        rospy.signal_shutdown("pose client is going to stop"); 

#!/usr/bin/env python

import roslib
import rospy
import smach
#import smach_ros
import numpy as np
from pose_client import Pose, Runner
from pid_client import PID

def main():
    rospy.init_node('VISION-KUKA-GRASP_state_machine')

    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=['finish'])
    sm.userdata.sm_counter = 0
    sm.userdata.dof_pos = np.zeros((4, 4))
    sm.userdata.dof_obj = ""

    # Open the container
    with sm:
        # Add states to the container

        smach.StateMachine.add('RUN_POSE', Runner(timeout=10), 
                               transitions={'vision':'POSE'})

        smach.StateMachine.add('POSE', Pose(timeout=10), 
                               transitions={'motion_planner':'PID',
                                            'interruption':'finish'},
                               remapping={'dof_position_out':'dof_pos',
                                          'dof_object_out':'dof_obj'})

        smach.StateMachine.add('PID', PID(timeout=10), 
                               transitions={'interruption':'finish'},
                               remapping={'dof_position':'dof_pos',
                                          'dof_object':'dof_obj'})


    # Execute SMACH plan
    outcome = sm.execute()
    rospy.spin()


if __name__ == '__main__':
    main()
"""
Author: Alexandr Gochakov (wandering@bk.ru)
Date: Mar 24, 2020
Сlient communication with PID
"""

import json
import rospy
import smach
from std_msgs.msg import String
import numpy as np


class PID(smach.State):
    def __init__(self, timeout=10):
        smach.State.__init__(self,
                             outcomes=['interruption'],
                             input_keys=['dof_position', 'dof_object'])
        self.timeout = timeout

    def execute(self,  userdata):
        rospy.loginfo('object: {}; pose: {}\n'.format(userdata.dof_object, userdata.dof_position))
        return 'interruption'
